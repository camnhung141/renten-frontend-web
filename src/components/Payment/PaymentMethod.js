
import CardLoad from "../Preloader/cardLoad";
import { useState } from "react";
import { getAddress, formatPrice, getTime } from "../../lib/utilities";

const PaymentMethod = ({orders, handlePaymentMethod, payment, method}) => {
    const [isCheck, setCheck] = useState(true);
    return (
        <div className="xVMPB">
            <div className="giRdMi">
                <h3 className="title">1. Chọn hình thức giao hàng</h3>
                <div className="frvXJv">
                    <div className="bPbcZg">
                        <div className="method-inner">
                            <div>
                                <label className="HafWE">
                                    <input
                                        type="radio"
                                        data-view-id="checkout.shipping_method_select"
                                        data-view-index="1"
                                        readOnly=""
                                        name="shipping-methods"
                                        value="1"
                                        checked={true}
                                        onChange={() => setCheck(true)}
                                    />
                                    <span className="radio-fake"></span>
                                    <span className="label">
                                        <div className="label-no-shippments">
                                            <div>
                                                <span className="method-name">
                                                    Giao Tiêu Chuẩn
                                    </span>
                                            </div>
                                        </div>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div className="fIiFCb">
                            {orders?.products?.length > 1 && (
                                <p className="message">
                                    Đơn hàng sẽ được giao 2 lần do có sản phẩm nằm ở
                                    các kho khác nhau
                                </p>
                            )}
                            {orders ? (
                                orders.products.map((item, key) => {
                                    const day = getTime(
                                        new Date(item.product.beginTime),   
                                        new Date(item.product.endTime),
                                        item.product.unitRent
                                    );
                                    const product = item.product;
                                    return (
                                        <div className="item" key={key}>
                                            <div className="item-left">
                                                <ul className="products">
                                                    <li className="product">
                                                        <div className="product__wrap">
                                                            <img
                                                                src={product.thumbnail}
                                                                alt={product.name}
                                                                className="product__img"
                                                            />
                                                            <div className="product__inner">
                                                                <p className="product__name">
                                                                    {product.name}
                                                                </p>
                                                                <p className="product__action">
                                                                    <span className="product__qty">
                                                                        TG: x{day}
                                                                    </span>
                                                                    <span className="product__pri">
                                                                        {formatPrice(
                                                                            day * product.rentPrice
                                                                        )}{" "}
                                                &nbsp;₫
                                              </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div className="includes-product"></div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="item-right">
                                                <div className="heading">
                                                    <div className="heading__sub">
                                                        <span className="heading__est-time">
                                                            Giao vào ngày nào đó
                                        </span>
                                                        <span className="heading__fulfil">
                                                            Được giao bởi {item.agencyName}
                                                        </span>
                                                    </div>
                                                    <div>
                                                        <span className="shipping-fee">
                                                            Free ship
                                        </span>
                                                    </div>
                                                </div>
                                                <div className="kERewZ">
                                                    <div className="sponsor__title">
                                                        <p className="title__name">
                                                            <span className="method-name">
                                                                Giao Tiêu Chuẩn
                                          </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            ) : (
                                    <CardLoad />
                                )}
                        </div>
                    </div>
                </div>
            </div>
            <div className="giRdMi">
                <h3 className="title">2. Chọn hình thức thanh toán</h3>
                <div className="dnENUJ">
                    <ul className="list">
                        <li className="dWHFNX">
                            <label className="HafWE">
                                <input
                                    type="radio"
                                    data-view-id="checkout.payment_method_select"
                                    data-view-index="cod"
                                    readOnly=""
                                    name="payment-methods"
                                    value="cash"
                                    checked={method === "cash"}
                                    onChange={(e) => handlePaymentMethod(e)}
                                />
                                <span className="radio-fake"></span>
                                <span className="label">
                                    <div className="fbjKoD">
                                        <img
                                            className="method-icon"
                                            width="32"
                                            src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/checkout/icon-payment-method-cod.svg"
                                            alt="cod"
                                        />
                                        <div className="method-content">
                                            <div className="method-content__name">
                                                <span>
                                                    Thanh toán tiền mặt khi nhận hàng
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </span>
                            </label>
                        </li>
                        <li className="dWHFNX">
                            <label className="HafWE">
                                <input
                                    type="radio"
                                    data-view-id="checkout.payment_method_select"
                                    data-view-index="momo"
                                    readOnly=""
                                    name="payment-methods"
                                    value="momo"
                                    checked={method === "momo"}
                                    onChange={(e) => handlePaymentMethod(e)}
                                />
                                <span className="radio-fake"></span>
                                <span className="label">
                                    <div className="fbjKoD">
                                        <img
                                            className="method-icon"
                                            width="32"
                                            src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/checkout/icon-payment-method-mo-mo.svg"
                                            alt="momo"
                                        />
                                        <div className="method-content">
                                            <div className="method-content__name">
                                                <span>Thanh toán bằng ví MoMo</span>
                                            </div>
                                        </div>
                                    </div>
                                </span>
                            </label>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="order-button">
                <button
                    data-view-id="checkout.confirmation_navigation_proceed"
                    className="btn btn-fill-out btn-block"
                    onClick={payment}
                >
                    ĐẶT THUÊ
                    </button>
                <p>
                    (Xin vui lòng kiểm tra lại đơn hàng trước khi Đặt Mua)
                    </p>
            </div>
        </div>
    )
}

export default PaymentMethod;
