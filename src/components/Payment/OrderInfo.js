

import CardLoad from "../Preloader/cardLoad";
import { useState } from "react";
import { getAddress, formatPrice, getTime } from "../../lib/utilities";

const OrderInfo = ({ delivery, orders }) => {
    const [isInfo, setInfo] = useState(false);

    return (
        <div className="evLkj">
            <div className="gDuXAE">
                <div className="title">
                    <span>Địa chỉ giao hàng</span>
                    <a
                        data-view-id="checkout.confirmation_shipping_location.change"
                        href="/checkout/shipping"
                    >
                        Sửa
                      </a>
                </div>
                <div className="address">
                    {orders ? (
                        <>
                            <span className="name">{delivery?.fullName}</span>
                            <span className="street">
                                {getAddress(delivery?.address)}
                            </span>
                            <span className="phone">
                                Điện thoại: {delivery.phoneNumber}
                            </span>
                        </>
                    ) : (
                            <CardLoad />
                        )}
                </div>
            </div>
            <div className="cpnhS">
                <div className="title">
                    <div className="sub-title">
                        <b>Đơn hàng</b>
                        <p>
                            {orders?.products.length} sản phẩm.{" "}
                            <span
                                className={`action ${isInfo && "action--active "}`}
                                onClick={() => setInfo(!isInfo)}
                            >
                                {isInfo ? "Thu gọn" : "Xem thông tin"}
                            </span>
                        </p>
                    </div>
                    <a
                        data-view-id="checkout.confirmation_cart_order.change"
                        href="/checkout/cart"
                    >
                        Sửa
                      </a>
                </div>
                <div className="cart">
                    {orders ? (
                        <>
                            <div
                                className={`product ${isInfo && "product--show "}`}
                            >
                                {orders.products.map((item, key) => {
                                    const product = item.product;
                                    const day = getTime(
                                        new Date(item.product.beginTime),
                                        new Date(item.product.endTime),
                                        item.product.unitRent
                                    );
                                    return (
                                        <div className="kEZgWN" key={key}>
                                            <div className="info">
                                                <strong className="qty">{day} x</strong>
                                                <a
                                                    data-view-id="checkout.confirmation_cart_order.product"
                                                    data-view-index="e7a87492-b8da-11eb-b8c1-7ec312856670"
                                                    href="/product-p57668290.html?spid=57668292"
                                                    target="_blank"
                                                    className="product-name"
                                                >
                                                    {product.name}
                                                </a>
                                                <span className="seller-name">
                                                    Cung cấp bởi{" "}
                                                    <strong>{item.agencyName}</strong>
                                                </span>
                                            </div>
                                            <div className="price">
                                                {formatPrice(day * product.rentPrice)}{" "}
                                    &nbsp;₫
                                  </div>
                                        </div>
                                    );
                                })}
                            </div>
                            <div className="price-summary">
                                <div className="cJLeJx">
                                    <div className="inner">
                                        <div className="name">Tạm tính</div>
                                        <div className="value">
                                            {formatPrice(orders?.totalPrice)} &nbsp;₫
                                </div>
                                    </div>
                                </div>
                                <div className="cJLeJx">
                                    <div className="inner">
                                        <div className="name">Giảm giá</div>
                                        <div className="value">-0.000 &nbsp;₫</div>
                                    </div>
                                </div>
                                <div className="cJLeJx">
                                    <div className="inner">
                                        <div className="name">Phí vận chuyển</div>
                                        <div className="value">Free ship</div>
                                    </div>
                                </div>
                                <div className="total">
                                    <div className="name">Thành tiền:</div>
                                    <div className="value">
                                        {formatPrice(orders?.totalPrice)} &nbsp;₫
                                <i>(Đã bao gồm VAT nếu có)</i>
                                    </div>
                                </div>
                            </div>{" "}
                        </>
                    ) : (
                            <CardLoad />
                        )}
                </div>
            </div>
        </div>
    )
}
export default OrderInfo;
