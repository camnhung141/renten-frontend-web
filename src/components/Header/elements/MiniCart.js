import { Fragment } from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";
import { IoIosClose } from "react-icons/io";
import { getDiscountPrice } from "../../../lib/product";
import { deleteFromCart } from "../../../redux/actions/cartActions";
import { formatPrice, getTime } from "../../../lib/utilities";

const MiniCart = ({ user, cartItems, deleteFromCart }) => {
  let cartTotalPrice = 0;
  const { addToast } = useToasts();
  return (
    <Fragment>
      {cartItems.length > 0 ? (
        <div className="cart-box">
          <ul className="cart-list">
            {cartItems.map((item, key) => {
              const dayBetween = getTime(
                new Date(item.beginTime),
                new Date(item.endTime),
                "date"
              );
              cartTotalPrice += item.product.rentPrice * dayBetween;
              return (
                <li key={key}>
                  <button
                    className="item-remove"
                    onClick={() => deleteFromCart(user, item.product, addToast)}
                  >
                    <IoIosClose />
                  </button>
                  <div className="single-item">
                    <div className="single-item__image">
                      <Link
                        href={{
                          pathname: "/product/[slug]",
                          query: {
                            slug: `${item.product.template.replaceAll(
                              " ",
                              "-"
                            )}`,
                            spid: item.product._id,
                          },
                        }}
                      >
                        <a>
                          <img
                            src={
                              item.product.thumbnail ||
                              "/assets/images/product/fashion/1.jpg"
                            }
                            alt="cart_thumb1"
                          />
                        </a>
                      </Link>
                    </div>
                    <div className="single-item__content">
                      <Link
                        href={{
                          pathname: "/product/[slug]",
                          query: {
                            slug: `${item.template?.name.replaceAll(" ", "-")}`,
                            spid: item.product._id,
                          },
                        }}
                      >
                        <a>{item.template?.name}</a>
                      </Link>
                      <span className="title-code">
                        {" "}
                        {item.product.productCode}
                      </span>
                      <span className="cart-quantity">
                        {" "}
                        {dayBetween} ngày x{" "}
                        {formatPrice(item.product.rentPrice)}
                        <span className="cart-amount">
                          {" "}
                          <span className="price-symbol">đ</span>
                        </span>
                      </span>
                      {item.product.selectedProductColor &&
                      item.product.selectedProductSize ? (
                        <div className="cart-variation">
                          <p>Color: {item.product.selectedProductColor}</p>
                          <p>Size: {item.product.selectedProductSize}</p>
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
          <div className="cart-footer">
            <p className="cart-total">
              <strong>Tổng:</strong> {formatPrice(cartTotalPrice)}
              <span className="cart-price">
                {" "}
                <span className="price-symbol">đ</span>
              </span>
            </p>
            <div className="cart-buttons">
              <Link href="/checkout/cart">
                <a className="btn btn-fill-line view-cart">Giỏ hàng</a>
              </Link>
              <Link href="/checkout/payment">
                <a className="btn btn-fill-out checkout">Thanh toán</a>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    deleteFromCart: (user, item, addToast) => {
      dispatch(deleteFromCart(user, item, addToast));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MiniCart);
