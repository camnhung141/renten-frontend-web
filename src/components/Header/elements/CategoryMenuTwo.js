import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Link from "next/link";
import { SlideDown } from "react-slidedown";
import { IoIosMenu, IoIosArrowForward } from "react-icons/io";

const CategoryMenuTwo = ({ categories, categoryMenuStyle }) => {
  const [categoryMenuExpandStatus, setCategoryMenuExpandStatus] =
    useState(true);
  const [categoryMenuItemExpandStatus, setCategoryMenuItemExpandStatus] =
    useState(false);

  return (
    <div className="header-categories-wrap">
      <button
        className={`category-menu-trigger ${
          categoryMenuStyle ? categoryMenuStyle : ""
        }`}
        onClick={() => setCategoryMenuExpandStatus(!categoryMenuExpandStatus)}
      >
        <IoIosMenu /> DANH MỤC
      </button>
      <nav className="category-menu dark-skin">
        <SlideDown closed={categoryMenuExpandStatus ? false : true}>
          <ul>
            {categories?.map((element) => (
              <li key={element._id} className="has-children-mega">
                <Link
                  href={{
                    pathname: "/[category]",
                    query: {
                      category: `${element.name.replaceAll(" ", "-")}`,
                      ctg: element._id,
                    },
                  }}
                >
                  <a className="nav-link">
                    {/* <i className="flaticon-tv"></i>{" "} */}
                    <img className="mx-2" width={25} src={element.icon}></img>
                    <span>
                      {element.name} <IoIosArrowForward />
                    </span>
                  </a>
                </Link>
                {element.subCategories?.length !== 0 && (
                  <ul className="sub-menu sub-menu--category sub-menu--category--with-banner sub-menu--mega">
                    <li className="sub-menu--mega__column">
                      <h3 className="sub-menu--mega__title">Nổi bật</h3>
                      <ul className="sub-menu--mega__list">
                        {element.subCategories.map((sub) => (
                          <li key={sub._id}>
                            <Link
                              href={{
                                pathname: "/[category]",
                                query: {
                                  category: `${sub?.name.replaceAll(" ", "-")}`,
                                  stg: sub._id,
                                },
                              }}
                            >
                              <a>{sub?.name}</a>
                            </Link>
                          </li>
                        ))}
                      </ul>
                    </li>
                  </ul>
                )}
              </li>
            ))}

            {/* <SlideDown closed={categoryMenuItemExpandStatus ? false : true}>
              <li>
                <Link
                  href="/[category]?category=may-tinh"
                  as="/may-tinh/?spm=1"
                >
                  <a className="nav-link">
                    <i className="flaticon-watch"></i> <span>Đồng hồ</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link
                  href="/[category]?category=may-tinh"
                  as="/may-tinh/?spm=1"
                >
                  <a className="nav-link">
                    <i className="flaticon-music-system"></i>{" "}
                    <span>Rạp và thiết bị</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link
                  href="/[category]?category=may-tinh"
                  as="/may-tinh/?spm=1"
                >
                  <a className="nav-link">
                    <i className="flaticon-monitor"></i> <span>Tivi</span>
                  </a>
                </Link>
              </li>
              <li>
                <Link
                  href="/[category]?category=may-tinh"
                  as="/may-tinh/?spm=1"
                >
                  <a className="nav-link">
                    <i className="flaticon-printer"></i>{" "}
                    <span>Thiết bị điện tử</span>
                  </a>
                </Link>
              </li>
            </SlideDown>
            <li>
              <button
                className="category-menu-expand-trigger"
                onClick={() =>
                  setCategoryMenuItemExpandStatus(!categoryMenuItemExpandStatus)
                }
              >
                Xem thêm <span>+</span>{" "}
              </button>
            </li> */}
          </ul>
        </SlideDown>
      </nav>
    </div>
  );
};
const mapStateToProps = (state) => {
  const categories = state.categoryData.categories;
  return {
    categories,
  };
};
export default connect(mapStateToProps)(CategoryMenuTwo);
