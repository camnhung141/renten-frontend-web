import { Fragment, useState } from "react";
import { IoIosSearch } from "react-icons/io";
import { connect } from "react-redux";
import { useRouter } from "next/router";

const Search = ({ categories, searchStyle }) => {
  const [keyword, setKeyword] = useState("");
  const [category, setCategory] = useState("");
  const router = useRouter();
  const handleSearch = () => {
    if (keyword ) {
      router.push({
        pathname: "/[category]",
        query: { category: "search", ctg: category, q: keyword },
      });
    }
  };

  return (
    <Fragment>
      <div
        className={`product-search-form product-search-form--style-two d-none d-lg-block ${
          searchStyle ? +searchStyle : ""
        }`}
      >
          <div className="input-group">
            <div className="input-group-prepend">
              <div className="custom-select-wrapper">
                <select
                  className="first-null"
                  onChange={(e) => setCategory(e.target.value)}
                >
                  <option value ={undefined}>Danh mục</option>
                  {categories?.map((category) => (
                    <option key={category._id} value={category._id}>
                      {category.name}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <input
              className="form-control"
              placeholder="Tìm sản phẩm, danh mục hay thương hiệu mong muốn ..."
              onChange={(e) => {
                setKeyword(e.target.value);
              }}
              type="text"
            />
            <button className="search-btn" onClick={()=>handleSearch()}>
              <IoIosSearch />
            </button>
          </div>
      </div>
    </Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    categories: state.categoryData.categories,
  };
};

export default connect(mapStateToProps)(Search);
