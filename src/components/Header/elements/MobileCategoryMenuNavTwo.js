import { useEffect } from "react";
import Link from "next/link";
import { connect } from "react-redux";

const MobileCategoryMenuNavTwo = ({ categories, getActiveStatus }) => {
  useEffect(() => {
    const offCanvasNav = document.querySelector(
      "#offcanvas-mobile-category-menu__navigation"
    );
    const offCanvasNavSubMenu = offCanvasNav.querySelectorAll(
      ".mobile-sub-menu"
    );
    const anchorLinks = offCanvasNav.querySelectorAll("a");

    for (let i = 0; i < offCanvasNavSubMenu.length; i++) {
      offCanvasNavSubMenu[i].insertAdjacentHTML(
        "beforebegin",
        "<span class='menu-expand'><i></i></span>"
      );
    }

    const menuExpand = offCanvasNav.querySelectorAll(".menu-expand");
    const numMenuExpand = menuExpand.length;

    for (let i = 0; i < numMenuExpand; i++) {
      menuExpand[i].addEventListener("click", (e) => {
        sideMenuExpand(e);
      });
    }

    for (let i = 0; i < anchorLinks.length; i++) {
      anchorLinks[i].addEventListener("click", () => {
        getActiveStatus(false);
      });
    }
  });

  const sideMenuExpand = (e) => {
    e.currentTarget.parentElement.classList.toggle("active");
  };
  return (
    <nav
      className="offcanvas-mobile-menu__navigation space-mb--30"
      id="offcanvas-mobile-category-menu__navigation"
    >
      <ul>
        {categories?.map((element) => (
          <li key={element._id} className="menu-item-has-children">
            <Link
              href={{
                pathname: '/[category]',
                query: { category: `${element.name.replaceAll(" ", "-")}`, ctg: element._id },
              }}
            >
              <a> {element.name} </a>
            </Link>
            {element.subCategories?.length !== 0 && (
              <ul className="mobile-sub-menu">
                {element.subCategories.map((sub) => (
                  <li key={sub._id} className="menu-item-has-children">
                    <Link
                      href={{
                        pathname: '/[category]',
                        query: { category: `${sub?.name.replaceAll(" ", "-")}`, stg: sub._id },
                      }}
                    >
                      <a>{sub?.name}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            )}
          </li>
        ))}
      </ul>
    </nav>
  );
};

const mapStateToProps = (state) => {
  const categories = state.categoryData.categories;
  return {
    categories
  };
};
export default connect(mapStateToProps)(MobileCategoryMenuNavTwo);
