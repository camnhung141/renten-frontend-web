import React, { useEffect, useState } from "react";
import Link from "next/link";
import { BsPerson } from "react-icons/bs";
import { FaFacebookF, FaGooglePlusG } from "react-icons/fa";
import accountService from "../../../apis/account.api";
import { useRouter } from "next/router";
import GoogleLogin from "react-google-login";
import FacebookLogin from "react-facebook-login";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { logout, fetchProfile } from "../../../redux/actions/customerActions";
import { logoutCart } from "../../../redux/actions/cartActions";
import { fetchCarts } from "../../../redux/actions/cartActions";
import { AuthToken } from "../../../apis/auth_token";
import Cookie from "js-cookie";
const UserDropDown = ({
  user,
  handleLogout,
  getProfile,
  getCarts,
  handleLogoutCart,
}) => {
  const router = useRouter();
  const token = Cookie.get("refreshToken");
  const userAuth = new AuthToken(token);
  const responseGoogle = async (response) => {
    // if (response?.profileObj) {
    //   const data = {
    //     user: response.profileObj.email,
    //     name: response.profileObj.name,
    //     password: response.googleId,
    //     role: "user",
    //   };
    //   loginSocial(data);
    // }
  };
  const responseFacebook = async (response) => {
    // if (response) {
    //   const data = {
    //     user: response.email,
    //     name: response.name,
    //     role: "user",
    //     password: response.id,
    //   };
    //   loginSocial(data);
    // }
  };
  useEffect(() => {
    if (!user && userAuth.isAuthenticated && token !== undefined) {
      getProfile();
      getCarts();
    }

    if (
      (token !== undefined && userAuth.isExpired) ||
      (user && userAuth.isExpired)
    ) {
      logoutClick();
    }
  }, [token]);
  const logoutClick = () => {
    // router.push("/");
    accountService.logout();
    handleLogout();
    handleLogoutCart();
  };

  return (
    <div className="nav-link pr-3">
      <div className="dropdown">
        <div className="dropbtn">
          <BsPerson />
          <span className="d-sm-none d-md-block mx-2">
            <span className="dropbtn-title">
              {!user ? "Đăng Nhập / Đăng Ký" : "Tài khoản"} <br />{" "}
              <span className="fz-13">
                {!user ? "Tài khoản" : user.fullName}
              </span>{" "}
            </span>
          </span>
        </div>
        {!user && (
          <div className="dropdown-content">
            <Link href="/renter/login">
              <a className="content-btn" href="#">
                Đăng nhập
              </a>
            </Link>
            <Link href="/renter/register">
              <a className="content-btn" href="#">
                Tạo tài khoản
              </a>
            </Link>

            <FacebookLogin
              appId="190986092743852"
              autoLoad={false}
              fields="name,email,picture"
              callback={responseFacebook}
              version="1.0"
              cssClass="content-btn btn btn-facebook"
              icon={false}
              textButton={
                <>
                  <FaFacebookF />
                  Đăng nhập bằng Facebook
                </>
              }
            ></FacebookLogin>
            <GoogleLogin
              clientId="188207156980-eb6drg36uns7clqveg0nduiq5ckgt4a3.apps.googleusercontent.com"
              className="btnGoogle"
              icon={false}
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
            >
              <a className="content-btn btn btn-google">
                <FaGooglePlusG /> Đăng nhập bằng Google
              </a>
            </GoogleLogin>
          </div>
        )}

        {user && (
          <div className="dropdown-content">
            <Link href="/renter/order/history">
              <a className="auth-btn"> Đơn hàng của tôi</a>
            </Link>
            {/* <Link href="/renter/profile#notice">
              <a className="auth-btn"> Thông báo của tôi</a>
            </Link> */}
            <Link href="/renter/account/edit">
              <a className="auth-btn"> Tài khoản của tôi</a>
            </Link>
            <Link href="/renter/feedback">
              <a className="auth-btn"> Nhận xét sản phẩm đã thuê</a>
            </Link>
            <Link href="/">
              <a onClick={logoutClick} className="auth-btn">
                Thoát tài khoản
              </a>
            </Link>
            {/* <button onClick={() => handleLogout()} className="auth-btn btn-logout">
              Thoát tài khoản
            </button> */}
          </div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
    pending: state.userData.pending,
    error: state.userData.error,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getProfile: fetchProfile,
      handleLogout: logout,
      getCarts: fetchCarts,
      handleLogoutCart: logoutCart,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(UserDropDown);
