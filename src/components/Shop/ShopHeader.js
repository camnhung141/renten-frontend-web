import { BsChatSquareFill } from "react-icons/bs";
import { IoIosSearch } from "react-icons/io";
import { useState } from "react";
import { useRouter } from "next/router";

const ShopHeader = ({ shop, query }) => {
  const home = query.t === "store" || !query.t;
  const products = query.t === "products";
  const info = query.t === "info";
  const [search, setSearch] = useState("");
  const router = useRouter();

  const handleClick = (keyword) => {
    router.replace({
      pathname: "/shop/[shop]",
      query: {
        shop: shop.agencyName.replaceAll(" ", "-"),
        sid: shop.accountId._id,
        t: keyword,
      },
    });
  };

  const handleSearch = () => {
    router.replace({
      pathname: "/shop/[shop]",
      query: {
        shop: shop.agencyName.replaceAll(" ", "-"),
        sid: shop.accountId._id,
        t: "products",
        q: search,
      },
    });
  };
  return (
    <div className="ggXoaf">
      <div id="seller-info-wrapper" className="llumpY">
        <div className="fWUUoC">
          <img
            src={
              shop.avatar ||
              "https://salt.tikicdn.com/ts/seller/4b/54/1a/f385a79a716cb3505f152e7af8c769aa.png"
            }
            alt=""
            width="100%"
          />
        </div>
        <div className="hxfDRr">
          <div className="iTRSqC">{shop.agencyName}</div>
          <div className="kyWFPx">{shop.email}</div>
        </div>
        <div className="ckFDVs">
          <button className="jSQvnW">
            <div className="ecDJXS">
              <BsChatSquareFill />
            </div>
            Chat ngay
          </button>
        </div>
      </div>
      <div className="clGHfU">
        <div className=" iiJnql">
          <div className="ljonFq" onClick={() => handleClick("store")}>
            <div className={home ? "laZaAe" : "eeaGij"}>Cửa Hàng</div>
          </div>
          <div className="ljonFq" onClick={() => handleClick("products")}>
            <div className={products ? "laZaAe" : "eeaGij"}>
              Tất Cả Sản Phẩm
            </div>
          </div>
          <div className="ljonFq" onClick={() => handleClick("info")}>
            <div className={info ? "laZaAe" : "eeaGij"}>Hồ Sơ Cửa Hàng</div>
          </div>
        </div>
        <div className="widget fZFSxY">
          <div className="search-form">
            <form>
              <input
                required
                className="form-control h-100 py-2"
                placeholder="Tìm sản phẩm tại cửa hàng"
                type="text"
                onChange={(e) => setSearch(e.target.value)}
              />
              <button
                type="button"
                title="Subscribe"
                className="btn icon-search"
                name="button"
                value="button"
                onClick={handleSearch}
              >
                <IoIosSearch />
              </button>
            </form>
          </div>
        </div>
      </div>
      <div className="dmJkSo"></div>
    </div>
  );
};

export default ShopHeader;
