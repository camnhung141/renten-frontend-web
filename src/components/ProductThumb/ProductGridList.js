import { Fragment, useState } from "react";
import Link from "next/link";
import { Col } from "react-bootstrap";
import ProductModal from "./elements/ProductModal";
import { ProductRating } from "../Product";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { fetchDetailProducts } from "../../redux/actions/productActions";
import { formatPrice } from "../../lib/utilities";
const ProductGridList = ({
  product,
  discountedPrice,
  productPrice,
  cartItem,
  wishlistItem,
  compareItem,
  bottomSpace,
  addToCart,
  addToWishlist,
  deleteFromWishlist,
  addToCompare,
  deleteFromCompare,
  addToast,
  cartItems,
  sliderClass,
}) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [modalShow, setModalShow] = useState(false);
  const [colorImage, setColorImage] = useState("");

  return (
    <Fragment>
      <Col
        lg={3}
        sm={6}
        className={`${sliderClass ? sliderClass : ""} ${
          bottomSpace ? bottomSpace : ""
        }`}
      >
        <div className="product-grid">
          <div className="product-grid__image">
            <Link
              href={{
                pathname: "/product/[slug]",
                query: {
                  slug: `${product.name.replaceAll(" ", "-")}`,
                  spid: product._id,
                },
              }}
            >
              <a>
                <img
                  className="p-2"
                  src={
                    product.thumbnail || "assets/images/product/fashion/1.jpg"
                  }
                  alt="product_img1"
                />
              </a>
            </Link>
            <div className="product-grid__badge-wrapper">
              {!product.new ? <span className="pr-flash">Mới nhất</span> : ""}
              {product.rate.rateCount > 1000 ? (
                <span className="pr-flash bg-danger">Thuê nhiều</span>
              ) : (
                ""
              )}
              {product?.price?.discount ? (
                <span className="pr-flash bg-success">Giảm giá</span>
              ) : (
                ""
              )}
            </div>
            <div className="product-grid__action-box">
              <ul>
                <li>
                  {product.affiliateLink ? (
                    <a href={product.affiliateLink} target="_blank">
                      <i className="icon-action-redo" />
                    </a>
                  ) : product.variation && product.variation.length >= 1 ? (
                    <Link
                      href={{
                        pathname: "/product/[slug]",
                        query: {
                          slug: `${product.name.replaceAll(" ", "-")}`,
                          spid: product._id,
                        },
                      }}
                    >
                      <a>
                        <i className="icon-wrench" />
                      </a>
                    </Link>
                  ) : product.inStock && product.inStock > 0 ? (
                    <button
                      onClick={() => addToCart(product, addToast)}
                      disabled={
                        cartItem !== undefined &&
                        cartItem.quantity >= cartItem.stock
                      }
                      className={cartItem !== undefined ? "active" : ""}
                    >
                      <i className="icon-basket-loaded" />
                    </button>
                  ) : (
                    <button disabled>
                      <i className="icon-basket-loaded" />
                    </button>
                  )}
                </li>
                <li>
                  <button
                    onClick={
                      compareItem !== undefined
                        ? () => deleteFromCompare(product, addToast)
                        : () => addToCompare(product, addToast)
                    }
                    className={compareItem !== undefined ? "active" : ""}
                  >
                    <i className="icon-shuffle" />
                  </button>
                </li>
                <li>
                  <button
                    onClick={() => setModalShow(true)}
                    className="d-none d-lg-block"
                  >
                    <i className="icon-magnifier-add" />
                  </button>
                </li>
                <li>
                  <button
                    onClick={
                      wishlistItem !== undefined
                        ? () => deleteFromWishlist(product, addToast)
                        : () => addToWishlist(product, addToast)
                    }
                    className={wishlistItem !== undefined ? "active" : ""}
                  >
                    <i className="icon-heart" />
                  </button>
                </li>
              </ul>
            </div>
          </div>
          <div className="product-grid__info">
            <h6 className="product-title">
              <Link
                href={{
                  pathname: "/product/[slug]",
                  query: {
                    slug: product.name.replaceAll(" ", "-"),
                    spid: product._id,
                  },
                }}
              >
                <a>{product.name}</a>
              </Link>
            </h6>
            <div className="product-price">
              {product?.price?.discount ? (
                <Fragment>
                  <span className="price">₫{discountedPrice}</span>
                  <del>{formatPrice(productPrice)} ₫ / {product.rentDetail.unitRent === "date" ? "Ngày" : "Giờ"}</del>
                  <span className="on-sale">
                    {" "}
                    {product?.price?.discount * 100}%
                  </span>
                </Fragment>
              ) : (
                <span className="price">
                  {formatPrice(productPrice)} ₫ /{" "}
                  {product.rentDetail.unitRent === "date" ? "Ngày" : "Giờ"}
                </span>
              )}
            </div>
            <div className="rating-wrap">
              <ProductRating ratingValue={product.rate.rateAverage} />
              <span className="rating-num">({product.rate.rateCount})</span>
            </div>

            {product.variation ? (
              <div className="product-switch-wrap">
                <ul>
                  {product.variation.map((single, key) => {
                    return (
                      <li key={key}>
                        <button
                          style={{ backgroundColor: `${single.colorCode}` }}
                          onClick={() => setColorImage(single.image)}
                          className={
                            colorImage === single.image ? "active" : ""
                          }
                        />
                      </li>
                    );
                  })}
                </ul>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="product-list">
          <div className="product-list__image">
            <Link
              href={{
                pathname: "/product/[slug]",
                query: {
                  slug: `${product.name.replaceAll(" ", "-")}`,
                  spid: product._id,
                },
              }}
            >
              <a>
                <img
                  className="p-2"
                  src={
                    product.thumbnail || "assets/images/product/fashion/1.jpg"
                  }
                  alt="product_img1"
                />
              </a>
            </Link>
            <div className="product-grid__badge-wrapper">
              {!product.new ? <span className="pr-flash">Mới nhất</span> : ""}
              {product.rate.rateCount > 1000 ? (
                <span className="pr-flash bg-danger">Thuê nhiều</span>
              ) : (
                ""
              )}
              {product?.price?.discount ? (
                <span className="pr-flash bg-success">Giảm giá</span>
              ) : (
                ""
              )}
            </div>
          </div>
          <div className="product-list__info">
            <h6 className="product-title">
              <Link
                href={{
                  pathname: "/product/[slug]",
                  query: {
                    slug: `${product.name.replaceAll(" ", "-")}`,
                    spid: product._id,
                  },
                }}
              >
                <a>{product.name}</a>
              </Link>
            </h6>
            <div className="d-flex justify-content-between">
              <div className="product-price">
                {product?.price?.discount ? (
                  <Fragment>
                    <span className="price">₫{discountedPrice}</span>
                    <del>{formatPrice(productPrice)}&nbsp;₫</del>
                    <span className="on-sale">
                      {product?.price?.discount * 100}%
                    </span>
                  </Fragment>
                ) : (
                  <span className="price">
                    {formatPrice(productPrice)}&nbsp;₫ /{product.rentDetail.unitRent === "date" ? "Ngày" : "Giờ"}
                  </span>
                )}
              </div>
              <div className="rating-wrap">
                <ProductRating ratingValue={product.rate.rateAverage} />
                <span className="rating-num">({product.rate.rateCount})</span>
              </div>
            </div>
            <div className="product-description">
              {product.description?.slice(0, 145) + "..."}
            </div>
            {product.variation ? (
              <div className="product-switch-wrap">
                <ul>
                  {product.variation.map((single, key) => {
                    return (
                      <li key={key}>
                        <button
                          style={{ backgroundColor: `${single.colorCode}` }}
                          onClick={() => setColorImage(single.image)}
                          className={
                            colorImage === single.image ? "active" : ""
                          }
                        />
                      </li>
                    );
                  })}
                </ul>
              </div>
            ) : (
              ""
            )}
            <div className="product-list__actions">
              <ul>
                <li>
                  <Link
                    href={{
                      pathname: "/product/[slug]",
                      query: {
                        slug: `${product.name.replaceAll(" ", "-")}`,
                        spid: product._id,
                      },
                    }}
                  >
                    <a className="btn btn-fill-out btn-addtocart">
                      <i className="icon-basket-loaded" /> Chọn thuê
                    </a>
                  </Link>
                </li>
                <li>
                  <button
                    onClick={
                      compareItem !== undefined
                        ? () => deleteFromCompare(product, addToast)
                        : () => addToCompare(product, addToast)
                    }
                    className={compareItem !== undefined ? "active" : ""}
                  >
                    <i className="icon-shuffle" />
                  </button>
                </li>
                <li>
                  <button
                    onClick={() => setModalShow(true)}
                    className="d-none d-lg-block"
                  >
                    <i className="icon-magnifier-add" />
                  </button>
                </li>
                <li>
                  <button
                    onClick={
                      wishlistItem !== undefined
                        ? () => deleteFromWishlist(product, addToast)
                        : () => addToWishlist(product, addToast)
                    }
                    className={wishlistItem !== undefined ? "active" : ""}
                  >
                    <i className="icon-heart" />
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </Col>
      {/* product modal */}
      <ProductModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        product={product}
        discountedprice={discountedPrice}
        productPrice={productPrice}
        cartitems={cartItems}
        cartitem={cartItem}
        wishlistitem={wishlistItem}
        compareitem={compareItem}
        addtocart={addToCart}
        addtowishlist={addToWishlist}
        deletefromwishlist={deleteFromWishlist}
        addtocompare={addToCompare}
        deletefromcompare={deleteFromCompare}
        addtoast={addToast}
      />
    </Fragment>
  );
};

export default ProductGridList;
