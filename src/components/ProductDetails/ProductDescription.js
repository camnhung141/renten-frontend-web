import { Fragment, useState } from "react";
import Link from "next/link";
import { getProductCartQuantity, getProductStock } from "../../lib/product";
import { ProductRating } from "../Product";
import { BsShield, BsClock } from "react-icons/bs";
import { AiOutlineReload, AiOutlineShop } from "react-icons/ai";
import { GiSwapBag } from "react-icons/gi";
import {
  IoLogoFacebook,
  IoLogoTwitter,
  IoLogoGoogleplus,
  IoLogoYoutube,
  IoLogoInstagram,
} from "react-icons/io";
import { FaStar } from "react-icons/fa";
import { BsChatSquareFill } from "react-icons/bs";
import ModalTimeRental from "../Product/ModalTimeRental";
import { formatPrice, dateRange } from "../../lib/utilities";
const ProductDescription = ({
  user,
  product,
  productPrice,
  productCode,
  setProductCode,
  discountedPrice,
  cartItems,
  wishlistItem,
  compareItem,
  addToast,
  addToCart,
  addToWishlist,
  deleteFromWishlist,
  addToCompare,
  deleteFromCompare,
  productContentButtonStyleClass,
  ratingProducts,
}) => {
  const [time, setTime] = useState([]);
  const [selectedProductColor, setSelectedProductColor] = useState(
    product.variation ? product.variation[0].color : ""
  );
  const [selectedProductSize, setSelectedProductSize] = useState(
    product.variation ? product.variation[0].size[0].name : ""
  );
  const productStock = getProductStock(product);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const dateDisable = dateRange(ratingProducts, productCode);

  const handleTimeRental = (time) => {
    setTime(time);
    setShow(false);
  };
  const handleAddToCart = () => {
    const item = cartItems.filter(
      (cartItem) => cartItem.productId === productCode._id
    )[0];
    const beginTime = time[0];
    const endTime = time[1];

    if (item) {
      addToast("Sản phẩm đã có trong giỏ hàng.", {
        appearance: "warning",
        autoDismiss: true,
      });
    } else {
      addToCart(user, productCode, addToast, {
        beginTime: beginTime,
        endTime: endTime,
      });
    }
  };
  return (
    <div className="product-content">
      <p className="product-content__brand mb-1">
        {" "}
        Thương hiệu: <span>{product.brand}</span>
      </p>
      <span className="product-content__title space-mb--10">
        {product.name}
      </span>
      <div className="product-content__price-rating-wrapper space-mb--10 mt-3">
        <div className="product-content__price d-flex-align-items-center">
          {product?.price?.discount ? (
            <Fragment>
              <span className="price">{discountedPrice}&nbsp;₫</span>
              <del>{formatPrice(productPrice)}&nbsp;₫</del>
              <span className="on-sale">
                Giảm {product?.price?.discount * 100}%
              </span>
            </Fragment>
          ) : (
            <Fragment>
              <span className="price">
                {formatPrice(productCode?.originalPrice && product.rentPrice)}
                &nbsp;₫ /{" "}
                {product.rentDetail.unitRent === "date" ? "Ngày" : "Giờ"}
              </span>
              <p className="mb-0">
                Giá gốc: {formatPrice(product.originalPrice)} &nbsp;₫
              </p>
              <p className="pt-2">
                <b>Điểm yêu cầu: {product.rentDetail.requiredPoint} điểm</b>
              </p>
            </Fragment>
          )}
        </div>
        {product.rate && product.rate.rateAverage > 0 ? (
          <div className="product-content__rating-wrap">
            <div className="product-content__rating">
              <ProductRating ratingValue={product.rate?.rateAverage} />
              <span>({product.rate?.rateCount})</span>
              <span> | Đã thuê {product.rentCount}</span>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
      <div className="row">
        <div className="product-content__sort-info space-mb--20 col-lg-6">
          <ul>
            <li>
              <BsShield size={20} /> Chính sách thuê hàng theo quy định
            </li>
            <li>
              <AiOutlineReload size={20} /> Đổi và trả hàng theo chính sách của
              RentMart
            </li>
            <li>
              <GiSwapBag size={20} /> Cho phép thanh toán khi nhận sản phẩm
            </li>
          </ul>
        </div>
        <div className="product-shop space-mb--20 col-lg-6">
          <div>
            <div className="seller-info">
              <Link
                href={{
                  pathname: "/shop/[shop]",
                  query: {
                    shop: `${product.agency.agencyName.replaceAll(" ", "-")}`,
                    sid: product.agencyId,
                  },
                }}
              >
                <a className="overview">
                  <div className="logo">
                    <div className="picture">
                      <img
                        src={
                          product.agency.avatar ||
                          "https://vcdn.tikicdn.com/cache/w88/ts/seller/4b/54/1a/f385a79a716cb3505f152e7af8c769aa.png"
                        }
                        alt=""
                        className="PictureV2__StyledImage-tfuu67-1 hQOEQo"
                      />
                    </div>
                  </div>
                  <div className="overview-right center-item">
                    <span className="seller-name">
                      <span>{product.agency.agencyName}</span>
                    </span>
                  </div>
                </a>
              </Link>
            </div>
            <div className="seller-detail">
              <div className="item review">
                <div className="title">
                  <span>4.2 / 5.0</span>
                  <FaStar className="yellow" />
                </div>
                <div className="sub-title">973</div>
              </div>
              <div className="border-left"></div>
              <div className="item normal">
                <div className="title">
                  <span>147</span>
                </div>
                <div className="sub-title">Sản phẩm</div>
              </div>
            </div>
            <div className="seller-action ">
              <Link
                href={{
                  pathname: "/shop/[shop]",
                  query: {
                    shop: `${product.agency.agencyName.replaceAll(" ", "-")}`,
                    sid: product.agencyId,
                  },
                }}
              >
                <a className="action" data-view-id="pdp_store_seller.view">
                  <AiOutlineShop />
                  <span>Xem Shop</span>
                </a>
              </Link>
              <div
                data-view-id="pdp_store_seller.follow"
                data-view-label="11574"
                className="action follow"
              >
                <BsChatSquareFill />
                <span>Chat ngay</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr />
      <h6 className="title-product-code my-3">Mã sản phẩm: </h6>
      <div className="list-item-code ">
        {product.listProducts.map((item) => (
          <button
            key={item._id}
            onClick={() => setProductCode(item)}
            className={`item ${!(item._id === productCode._id) || "active"}`}
          >
            <h6>
              <i className="iconmobile-opt"></i>
              {item.productCode}
            </h6>
            <strong>
              {formatPrice(item.rentPrice)}&nbsp;₫ /{" "}
              {product.rentDetail.unitRent === "date" ? "Ngày" : "Giờ"}
            </strong>
          </button>
        ))}
      </div>
      <hr />
      <div className="time-rental">
        <button
          onClick={handleShow}
          className="btn btn-fill-out btn-timerental px-3 py-2"
        >
          <BsClock size={20} className="mr-1" />
          <span>Chọn thời gian thuê</span>
        </button>
        {time.length > 0 && (
          <h6 className="mt-2">
            <span className="mr-2">Thời gian:</span>
            {new Date(time[0]).toLocaleString("vn-VN")} -
            {new Date(time[1]).toLocaleString("vn-VN")}
          </h6>
        )}
        <ModalTimeRental
          show={show}
          handleClose={handleClose}
          handleSave={handleTimeRental}
          dateDisable={dateDisable}
        />
      </div>
      <hr />
      {product.affiliateLink ? (
        <div className="product-content__quality">
          <div className="product-content__cart btn-hover">
            <a
              href={product.affiliateLink}
              target="_blank"
              className="btn btn-fill-out btn-addtocart"
            >
              Thuê ngay
            </a>
          </div>
        </div>
      ) : (
        <Fragment>
          <div
            className={`${
              productContentButtonStyleClass
                ? productContentButtonStyleClass
                : "product-content__button-wrapper d-flex align-items-center"
            }`}
          >
            {/* {productStock && productStock > 0 ? ( */}
            <button
              onClick={handleAddToCart}
              // disabled={productCode.rented}
              className="btn btn-fill-out btn-addtocart "
            >
              <i className="icon-basket-loaded" />
              Chọn Thuê
              {/* {productCode.rented ? "Đã được thuê" : "Chọn Thuê"} */}
            </button>
            {/* ) : (
              <button className="btn btn-fill-out btn-addtocart" disabled>
                Hết hàng
              </button>
            )} */}

            <button
              className={`product-content__compare ${
                compareItem !== undefined ? "active" : ""
              }`}
              title={
                compareItem !== undefined
                  ? "Added to compare"
                  : "Add to compare"
              }
              onClick={
                compareItem !== undefined
                  ? () => deleteFromCompare(product, addToast)
                  : () => addToCompare(product, addToast)
              }
            >
              <i className="icon-shuffle" />
            </button>

            <button
              className={`product-content__wishlist ${
                wishlistItem !== undefined ? "active" : ""
              }`}
              title={
                wishlistItem !== undefined
                  ? "Added to wishlist"
                  : "Add to wishlist"
              }
              onClick={
                wishlistItem !== undefined
                  ? () => deleteFromWishlist(product, addToast)
                  : () => addToWishlist(product, addToast)
              }
            >
              <i className="icon-heart" />
            </button>
          </div>
        </Fragment>
      )}
      <hr />
      <ul className="product-content__product-meta">
        <li>
          SKU: <span>{product.sku}</span>
        </li>
        <li>
          Danh mục:
          {product.categories &&
            product.categories.map((item, index, arr) => {
              return (
                <Link
                  href={`/[category]?category=${item.name.replaceAll(
                    " ",
                    "-"
                  )}`}
                  as={`/${item.name.replaceAll(" ", "-")}/?ctg=${item._id}`}
                  key={item._id}
                >
                  <a>{item.name + (index !== arr.length - 1 ? ", " : "")}</a>
                </Link>
              );
            })}
        </li>
        <li>
          Tags:
          {product.tags &&
            product.tags.map((item, index, arr) => {
              return (
                <Link
                  href="/[category]?category=may-tinh"
                  as="/may-tinh/?spm=1"
                  as={"/shop/grid-left-sidebar"}
                  key={index}
                >
                  <a>{item + (index !== arr.length - 1 ? ", " : "")}</a>
                </Link>
              );
            })}
        </li>
      </ul>
      <div className="product-content__product-share space-mt--15">
        <span>Chia sẻ:</span>
        <ul className="social-icons">
          <li>
            <a href="#">
              <IoLogoFacebook />
            </a>
          </li>
          <li>
            <a href="#">
              <IoLogoTwitter />
            </a>
          </li>
          <li>
            <a href="#">
              <IoLogoGoogleplus />
            </a>
          </li>
          <li>
            <a href="#">
              <IoLogoYoutube />
            </a>
          </li>
          <li>
            <a href="#">
              <IoLogoInstagram />
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default ProductDescription;
