import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import { IoIosStar, IoIosStarOutline } from "react-icons/io";
import parse from "html-react-parser";
import { formatDate, ratingCount } from "../../lib/utilities";
import { ProductRating } from "../Product";
import { FaStar, FaRegStar } from "react-icons/fa";

const ProductDescriptionTab = ({ product, reviews, ratingProducts }) => {
  const images = reviews.map((item) => item.images);
  const reviewsCount = reviews?.length;

  return (
    <div className="product-description-tab space-pt--20 space-pb--50">
      <Tab.Container defaultActiveKey="description">
        <Nav
          variant="pills"
          className="product-description-tab__navigation space-mb--50"
        >
          <Nav.Item>
            <Nav.Link eventKey="description">Mô tả sản phẩm</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="additionalInfo">Thông tin chi tiết</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="reviews">
              Đánh giá sản phẩm{" "}
              {product.rate.rateCount ? `(${product.rate.rateCount})` : ""}
            </Nav.Link>
          </Nav.Item>
        </Nav>
        <Tab.Content>
          <Tab.Pane eventKey="description">
            <div className="product-description-tab__details">
              {parse(product.description.replaceAll("\n", "<br/>"))}
            </div>
          </Tab.Pane>
          <Tab.Pane eventKey="additionalInfo">
            <div className="product-description-tab__additional-info">
              <table className="table table-bordered">
                <tbody>
                  <tr>
                    <td>Capacity</td>
                    <td>5 Kg</td>
                  </tr>
                  <tr>
                    <td>Color</td>
                    <td>Black, Brown, Red,</td>
                  </tr>
                  <tr>
                    <td>Water Resistant</td>
                    <td>Yes</td>
                  </tr>
                  <tr>
                    <td>Material</td>
                    <td>Artificial Leather</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </Tab.Pane>
          <Tab.Pane eventKey="reviews">
            <div className="product-description-tab__review">
              {reviews.length > 0 && (
                <div className="comments">
                  <div className="customer-reviews__top">
                    <div className=" pFROu review-rating">
                      <div className="review-rating__inner">
                        <div className="review-rating__summary">
                          <div className="review-rating__point">
                            {product.rate.rateAverage.toFixed(1)}
                          </div>
                          <div className="review-rating__stars">
                            <div className=" dYhelp">
                              <ProductRating
                                ratingValue={product.rate.rateAverage}
                              />
                            </div>
                            <div className="review-rating__total">
                              {product.rate.rateCount} nhận xét
                            </div>
                          </div>
                        </div>
                        <div className="review-rating__detail">
                          <div className="review-rating__level">
                            <div className=" dYhelp">
                              <ProductRating ratingValue={5} />
                            </div>
                            <div className=" KCfxa">
                              <div style={{ width: "72%" }}></div>
                            </div>
                            <div className="review-rating__number">
                              {ratingCount(reviews, 5)}
                            </div>
                          </div>
                          <div className="review-rating__level">
                            <div className=" dYhelp">
                              <ProductRating ratingValue={4} />
                            </div>
                            <div className=" KCfxa">
                              <div style={{ width: "0%" }}></div>
                            </div>
                            <div className="review-rating__number">
                              {ratingCount(reviews, 4)}
                            </div>
                          </div>
                          <div className="review-rating__level">
                            <div className=" dYhelp">
                              <ProductRating ratingValue={3} />
                            </div>
                            <div className=" KCfxa">
                              <div style={{ width: "14%" }}></div>
                            </div>
                            <div className="review-rating__number">
                              {ratingCount(reviews, 3)}
                            </div>
                          </div>
                          <div className="review-rating__level">
                            <div className=" dYhelp">
                              <ProductRating ratingValue={2} />
                            </div>
                            <div className=" KCfxa">
                              <div style={{ width: " 0%" }}></div>
                            </div>
                            <div className="review-rating__number">
                              {ratingCount(reviews, 2)}
                            </div>
                          </div>
                          <div className="review-rating__level">
                            <div className=" dYhelp">
                              <ProductRating ratingValue={1} />
                            </div>
                            <div className=" KCfxa">
                              <div style={{ width: "14%" }}></div>
                            </div>
                            <div className="review-rating__number">
                              {ratingCount(reviews, 1)}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div className=" kfqcjJ review-images">
                        <div className="review-images__heading">
                          Tất cả hình ảnh (4)
                        </div>
                        <div className="review-images__inner">
                          <div
                            className="review-images__item"
                            data-view-id="pdp_product_review_view_photo"
                          >
                            <div className="review-images__img"></div>
                          </div>
                        </div>
                      </div>
                      <div className=" fZjOyV filter-review">
                        <div className="filter-review__label">
                          Lọc xem theo :
                        </div>
                        <div className="filter-review__inner">
                          <div
                            data-view-id="pdp_review_filter_item"
                            data-view-index="0"
                            className="filter-review__item"
                          >
                            <span className="filter-review__check"></span>
                            <span className="filter-review__text">
                              Mới nhất
                            </span>
                          </div>
                          <div
                            data-view-id="pdp_review_filter_item"
                            data-view-index="1"
                            className="filter-review__item"
                          >
                            <span className="filter-review__check"></span>
                            <span className="filter-review__text">
                              Có hình ảnh
                            </span>
                          </div>
                          <div
                            data-view-id="pdp_review_filter_item"
                            data-view-index="3"
                            className="filter-review__item filter-review__item--active"
                          >
                            <span className="filter-review__check"></span>
                            <span className="filter-review__text">5</span>
                            <FaStar className="yellow" size={20} />
                          </div>
                          <div
                            data-view-id="pdp_review_filter_item"
                            data-view-index="4"
                            className="filter-review__item"
                          >
                            <span className="filter-review__check"></span>
                            <span className="filter-review__text">4</span>
                            <FaStar className="yellow" size={20} />
                          </div>
                          <div
                            data-view-id="pdp_review_filter_item"
                            data-view-index="5"
                            className="filter-review__item"
                          >
                            <span className="filter-review__check"></span>
                            <span className="filter-review__text">3</span>
                            <FaStar className="yellow" size={20} />
                          </div>
                          <div
                            data-view-id="pdp_review_filter_item"
                            data-view-index="6"
                            className="filter-review__item filter-review__item--active"
                          >
                            <span className="filter-review__check"></span>
                            <span className="filter-review__text">2</span>
                            <FaStar className="yellow" size={20} />
                          </div>
                          <div
                            data-view-id="pdp_review_filter_item"
                            data-view-index="7"
                            className="filter-review__item"
                          >
                            <span className="filter-review__check"></span>
                            <span className="filter-review__text">1</span>
                            <FaStar className="yellow" size={20} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul className="list-none comment-list mt-4">
                    {reviews?.map((review, key) => (
                      <li key={key}>
                        <div className="comment-img">
                          <img
                            src={
                              review.avatar || "/assets/images/users/user1.jpg"
                            }
                            alt="user1"
                          />
                        </div>
                        <div className="comment-block">
                          <div className="rating-wrap">
                            <div className="rating">
                              <ProductRating ratingValue={review.rating} />
                            </div>
                          </div>
                          <p className="customer-meta">
                            <span className="review-author">{review.name}</span>
                            <span className="comment-date">
                              Nhận xét vào {formatDate(review.timeCreated)}
                            </span>
                          </p>
                          <div className="description">
                            <p>{review.content}</p>
                          </div>
                          <div className="review-comment__images">
                            {review.images?.map((image, key) => {
                              const url = `url("${image}")`;
                              return (
                                <div
                                  key={key}
                                  className="review-comment__image"
                                  style={{ backgroundImage: url }}
                                ></div>
                              );
                            })}
                          </div>
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              )}
              {reviews.length === 0 && (
                <div className="customer-reviews__inner">
                  <div className="customer-reviews__empty">
                    <FaStar className="yellow" size={50} />
                    <span>
                      Chưa có đánh giá nào cho sản phầm này từ{" "}
                      <b>{product.agency.agencyName}</b>
                    </span>
                  </div>
                </div>
              )}
            </div>
          </Tab.Pane>
        </Tab.Content>
      </Tab.Container>
    </div>
  );
};

export default ProductDescriptionTab;
