import { Fragment, useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import Swiper from "react-id-swiper";
import { LightgalleryProvider, LightgalleryItem } from "react-lightgallery";

const ImageGalleryLeftThumb = ({ product }) => {
  const [gallerySwiper, getGallerySwiper] = useState(null);
  const [thumbnailSwiper, getThumbnailSwiper] = useState(null);
  // effect for swiper slider synchronize
  useEffect(() => {
    if (
      gallerySwiper !== null &&
      gallerySwiper.controller &&
      thumbnailSwiper !== null &&
      thumbnailSwiper.controller
    ) {
      gallerySwiper.controller.control = thumbnailSwiper;
      thumbnailSwiper.controller.control = gallerySwiper;
    }
  }, [gallerySwiper, thumbnailSwiper]);

  // swiper slider settings
  const length = product?.images?.length || 5;
  const gallerySwiperParams = {
    getSwiper: getGallerySwiper,
    spaceBetween: 10,
    loopedSlides: 4,
    loop: true,
    effect: "fade"
  };

  const thumbnailSwiperParams = {
    getSwiper: getThumbnailSwiper,
    spaceBetween: 10,
    slidesPerView: length,
    loopedSlides: 4,
    touchRatio: 0.2,
    loop: true,
    slideToClickedSlide: true,
    direction: "vertical",
    breakpoints: {
      1200: {
        slidesPerView: length,
        direction: "vertical",
      },
      992: {
        slidesPerView: length,
        direction: "horizontal",
      },
      768: {
        slidesPerView: length,
        direction: "horizontal",
      },
      640: {
        slidesPerView: length,
        direction: "horizontal",
      },
      320: {
        slidesPerView: length,
        direction: "horizontal",
      },
    },
  };

  return (
    <Fragment>
      <Row className="image-gallery-side-thumb-wrapper">
        <Col xl={10} className="order-1 order-xl-2">
          <div className="product-large-image-wrapper">
            <LightgalleryProvider>
              <Swiper {...gallerySwiperParams}>
                {product?.images?.map((single, index) => {
                  return (
                    <div key={index}>
                      <LightgalleryItem group="any" src={single}>
                        <button className="enlarge-icon">
                          <i className="icon-magnifier-add" />
                        </button>
                      </LightgalleryItem>
                      <div className="single-image">
                        <img src={single} className="img-fluid" alt="" />
                      </div>
                    </div>
                  );
                })}
              </Swiper>
            </LightgalleryProvider>
          </div>
        </Col>
        <Col xl={2} className="order-2 order-xl-1">
          <div className="product-small-image-wrapper product-small-image-wrapper--side-thumb">
            <Swiper {...thumbnailSwiperParams}>
              {product?.images?.map((image, index) => {
                return (
                  <div key={index}>
                    <div className="single-image">
                      <img src={image} className="img-fluid" alt="" />
                    </div>
                  </div>
                );
              })}
            </Swiper>
          </div>
        </Col>
      </Row>
    </Fragment>
  );
};

export default ImageGalleryLeftThumb;
