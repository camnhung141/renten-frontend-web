import { BsFilterRight } from "react-icons/bs";
import React, { Fragment, useState } from "react";

const FilterOrder = ({ orders, setOrderFilter }) => {
  const [open, setOpenFilter] = useState(false);
  const handleFilter = (status) => {
    if (status && orders) {
      setOrderFilter(orders.filter((item) => item.status === status));
      setOpenFilter(false);
    }
    if (status === "all") {
      setOrderFilter(orders);
    }
  };
  return (
    <div className="filter-order">
      <button onClick={() => setOpenFilter(!open)}>
        <BsFilterRight size={25} />
      </button>
      <div className={open ? "fcbiuR" : "fUkGOD"}>
        <div onClick={() => handleFilter("all")}>Tất cả</div>
        <div onClick={() => handleFilter("waitingToConfirm")}>Đợi xác nhận</div>
        <div onClick={() => handleFilter("processing")}>Đã xác nhận</div>
        <div onClick={() => handleFilter("transporting")}>Đang vận chuyển</div>
        <div onClick={() => handleFilter("rented")}>Đã thuê</div>
        <div onClick={() => handleFilter("returned")}>Đã hoàn trả</div>
        <div onClick={() => handleFilter("cancel")}>Đã huỷ</div>
        <div onClick={() => handleFilter("late")}>Trả chậm</div>
        <div onClick={() => handleFilter("notReturned")}>Không hoàn trả</div>


      </div>
    </div>
  );
};
export default FilterOrder;
