import { Col } from "react-bootstrap";
import Nav from "react-bootstrap/Nav";
import MenuLinks from "./config";

export default function NavBar() {
  return (
    <>
      <Col lg={3} md={4}>
        <Nav
          variant="pills"
          className="flex-column my-account-content__navigation space-mb--r60"
        >
          {MenuLinks.items.map((item) => (
            <Nav.Item key={item.eventKey}>
              <Nav.Link href={item.href} eventKey={item.eventKey}>
                {item.icon} {item.title}
              </Nav.Link>
            </Nav.Item>
          ))}
        </Nav>
      </Col>
    </>
  );
}
