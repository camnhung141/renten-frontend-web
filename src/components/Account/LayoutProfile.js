import Link from "next/link";
import { LayoutSix } from "../../layouts";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import { Container, Row, Tab, Card, Col } from "react-bootstrap";
import NavBar from "./NavBar";
import React, { Fragment, useState } from "react";
import FilterOrder from "./FilterOrder";

const LayoutProfile = ({
  title,
  children,
  breadcrumbName,
  activeKey,
  orders,
  setOrderFilter,
}) => {
  return (
    <LayoutSix title={title}>
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle="Tài khoản cá nhân">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="#">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">{breadcrumbName}</li>
        </ol>
      </BreadcrumbOne>
      <div className="my-account-content space-pt--30 space-pb--r100">
        <Container>
          <Tab.Container defaultActiveKey={activeKey}>
            <Row>
              <NavBar />
              <Col lg={9} md={8}>
                <Tab.Content>
                  <Card className="my-account-content__content">
                    <Card.Header>
                      <p className="title-card-header mb-0 d-inline">
                        {breadcrumbName}
                      </p>
                      {activeKey === "order" && (
                        <FilterOrder
                          orders={orders}
                          setOrderFilter={setOrderFilter}
                        />
                      )}
                    </Card.Header>
                    <Card.Body className={`${activeKey}`}>{children}</Card.Body>
                  </Card>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </Container>
      </div>
    </LayoutSix>
  );
};

export default LayoutProfile;
// export default withPrivateRoute(connect(mapStateToProps)(MyAccount));
