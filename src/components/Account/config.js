import React from "react";
import {
  FaCloudDownloadAlt,
  FaRegEdit,
  FaHistory,
  FaBell,
  FaShippingFast
} from "react-icons/fa";
import {
  IoIosList,
  IoIosClipboard,
  IoIosDownload,
  IoIosCash,
  IoIosCreate,
  IoIosPerson,
  IoIosPin,
} from "react-icons/io";
import { RiLockPasswordLine } from "react-icons/ri";
import { MdPayment, MdVerifiedUser } from "react-icons/md";

const ICONS = {
  accountDetails: <IoIosPerson />,
  verify: <MdVerifiedUser />,
  order: <FaShippingFast />,
  download: <IoIosDownload />,
  payment: <MdPayment />,
  address: <IoIosPin />,
  changePassword: <RiLockPasswordLine />,
  notice: <FaBell/>
};

export default {
  items: [
    {
      title: "Thông tin cá nhân",
      href: "/renter/account/edit",
      eventKey: "accountDetails",
      icon: ICONS.accountDetails,
    },
    {
      title: "Xác thực tài khoản",
      href: "/renter/account/verify",
      eventKey: "verify",
      icon: ICONS.verify,
    },
    {
      title: "Thông báo của thôi",
      href: "/renter/notification",
      eventKey: "notice",
      icon: ICONS.notice,
    },
    {
      title: "Quản lý đơn hàng",
      href: "/renter/order/history",
      eventKey: "order",
      icon: ICONS.order,
    },
    // {
    //   title: "Đã lưu",
    //   href: "#download",
    //   eventKey: "download",
    //   icon: ICONS.download,
    // },
    // {
    //   title: "Thanh toán",
    //   href: "#payment",
    //   eventKey: "payment",
    //   icon: ICONS.payment,
    // },
    {
      title: "Sổ địa chỉ",
      href: "/renter/address",
      eventKey: "address",
      icon: ICONS.address,
    },
    {
      title: "Đổi mật khẩu",
      href: "/renter/account/change-password",
      eventKey: "changePassword",
      icon: ICONS.changePassword,
    },
  ],
};
