import React from "react";
import { FaRegEdit } from "react-icons/fa";
import { IoMdAdd } from "react-icons/io";
import { AiOutlineDelete } from "react-icons/ai";
import Link from "next/link";
import { FiCheckCircle } from "react-icons/fi";

export default function Address({ address, handleDelete }) {
  return (
    <>
      <address className="card-address">
        <p>
          <strong>{address.fullName}</strong>
          {!address.selected || (
            <>
              <FiCheckCircle color="green" className="ml-3 mr-1" />{" "}
              <span>Địa chỉ mặc định </span>
            </>
          )}
        </p>
        <p>
          {" "}
          Địa chỉ:{" "}
          <span className="address">
            {address.streetAndNumber} , {address.wardName},{" "}
            {address.districtName}, {address.cityName}
          </span>{" "}
        </p>
        <p>
          Điện thoại: <span className="address">{address.phoneNumber}</span>
        </p>
        <Link
          href={{
            pathname: "/renter/address/[address]",
            query: {
              address: "edit",
              aid: address._id,
            },
          }}
        >
          <a className="check-btn sqr-btn ">
            <FaRegEdit /> Chỉnh sửa
          </a>
        </Link>
        {address.selected || (
          <button
            onClick={handleDelete}
            className="check-btn sqr-btn btn-delete "
          >
            <AiOutlineDelete /> Xoá
          </button>
        )}
      </address>
    </>
  );
}
