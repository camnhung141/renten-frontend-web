import { Fragment } from "react";
import { HeaderFive } from "../components/Header";
import { FooterSix } from "../components/Footer";
import { useEffect } from "react";

const LayoutFive = ({ title, children, navPositionClass }) => {

  useEffect(() => {
    document.title = title || "Rent Mart - Thuê đồ online giá tốt, hàng chuẩn, nhanh chóng";
  })

  return (
    <Fragment>
      <HeaderFive navPositionClass={navPositionClass} />
      {children}
      <FooterSix />
    </Fragment>
  );
};

export default LayoutFive;
