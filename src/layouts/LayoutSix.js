import { Fragment } from "react";
import { HeaderSix } from "../components/Header";
import { FooterSix } from "../components/Footer";
import { useEffect } from "react";
import { NextSeo } from 'next-seo';

const LayoutSix = ({ title, children, navPositionClass }) => {

  return (
    <Fragment>
      <NextSeo
      title= {title || "Rent Mart - Thuê đồ online giá tốt, hàng chuẩn, nhanh chóng"}
      description="A short description goes here."
    />
      <HeaderSix navPositionClass={navPositionClass} />
      {children}
      <FooterSix />
    </Fragment>
  );
};

export default LayoutSix;
