import Link from "next/link";
import { Container, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";
import { getDiscountPrice } from "../../lib/product";
import { getProducts } from "../../lib/product";
import { LayoutSix } from "../../layouts";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import { Fragment, useState, useEffect } from "react";

import {
  ImageGalleryLeftThumb,
  ImageGalleryBottomThumb,
  ProductDescription,
  ProductDescriptionTab,
} from "../../components/ProductDetails";
import { addToCart } from "../../redux/actions/cartActions";
import {
  addToWishlist,
  deleteFromWishlist,
} from "../../redux/actions/wishlistActions";
import {
  addToCompare,
  deleteFromCompare,
} from "../../redux/actions/compareActions";
import { ProductSliderTwo } from "../../components/ProductSlider";
import productService from "../../apis/product.api";
const ProductDetail = ({
  user,
  product,
  reviews,
  cartItems,
  wishlistItems,
  compareItems,
  addToCart,
  addToWishlist,
  deleteFromWishlist,
  addToCompare,
  deleteFromCompare,
  relatedProducts,
  ratingProducts,
}) => {
  const { addToast } = useToasts();
  const productPrice = product.rentPrice.toFixed(2);
  const cartItem = cartItems.filter(
    (cartItem) => cartItem.id === product.id
  )[0];
  const wishlistItem = wishlistItems.filter(
    (wishlistItem) => wishlistItem.id === product.id
  )[0];
  const compareItem = compareItems.filter(
    (compareItem) => compareItem.id === product.id
  )[0];
  const [productCode, setProductCode] = useState(product?.listProducts[0]);
  useEffect(() => {
    setProductCode(product?.listProducts[0]);
  }, [product]);
  return (
    <LayoutSix
      title={`${product.name} | ${product.agency.agencyName} | Rent Mart`}
    >
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle={product.name}>
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item">
            <Link
              href={{
                pathname: "/[category]",
                query: {
                  category: `${product.categories[0].name.replaceAll(
                    " ",
                    "-"
                  )}`,
                  ctg: product.categories[0]._id,
                },
              }}
            >
              <a>{product.categories[0].name}</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">{product.name}</li>
        </ol>
      </BreadcrumbOne>

      {/* product details */}
      <div className="product-details space-pt--20 space-pb--r100">
        <Container>
          <Row>
            <Col lg={6} className="space-mb-mobile-only--40">
              {/* image gallery */}
              <ImageGalleryBottomThumb
                productTemplate={product}
                code={productCode}
              />
              {/* <ImageGalleryLeftThumb product={product} code={productCode}/> */}
            </Col>
            <Col lg={6}>
              {/* product description */}
              <ProductDescription
                user={user}
                product={product}
                productPrice={productPrice}
                productCode={productCode}
                setProductCode={setProductCode}
                ratingProducts={ratingProducts}
                cartItems={cartItems}
                cartItem={cartItem}
                wishlistItem={wishlistItem}
                compareItem={compareItem}
                addToast={addToast}
                addToCart={addToCart}
                addToWishlist={addToWishlist}
                deleteFromWishlist={deleteFromWishlist}
                addToCompare={addToCompare}
                deleteFromCompare={deleteFromCompare}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              {/* product description tab */}
              <ProductDescriptionTab
                product={product}
                reviews={reviews}
                ratingProducts={ratingProducts}
              />
            </Col>
          </Row>

          {/* related product slider */}
          <ProductSliderTwo
            title="Sản phẩm tương tự"
            products={relatedProducts}
          />
        </Container>
      </div>
    </LayoutSix>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
    products: state.productData.products,
    cartItems: state.cartData,
    wishlistItems: state.wishlistData,
    compareItems: state.compareData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (
      user,
      item,
      addToast,
      timeRental,
      selectedProductColor,
      selectedProductSize
    ) => {
      dispatch(
        addToCart(
          user,
          item,
          addToast,
          timeRental,
          selectedProductColor,
          selectedProductSize
        )
      );
    },
    addToWishlist: (item, addToast) => {
      dispatch(addToWishlist(item, addToast));
    },
    deleteFromWishlist: (item, addToast) => {
      dispatch(deleteFromWishlist(item, addToast));
    },
    addToCompare: (item, addToast) => {
      dispatch(addToCompare(item, addToast));
    },
    deleteFromCompare: (item, addToast) => {
      dispatch(deleteFromCompare(item, addToast));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);

export async function getServerSideProps({ query }) {
  try {
    const product = await productService.getDetailProduct(query.spid);
    const response = await productService.getProducts({
      categoriesId: product?.categories[0]?._id,
      sort: "rentCount:-1",
      limit: 8,
    });
    const reviews = await productService.getReviewProduct(query.spid);
    const relatedProducts = response?.products;
    const ratingProducts = await productService.getRatingProducts(query.spid);
    return { props: { product, relatedProducts, reviews, ratingProducts } };
  } catch {
    return {
      redirect: {
        destination: "/not-found",
        permanent: false,
      },
    };
  }
}
