import Link from "next/link";
import { LayoutSix } from "../../layouts";
import { useRouter } from "next/router";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import { Container, Row, Col } from "react-bootstrap";
import { FaFacebookF, FaGooglePlusG } from "react-icons/fa";
import { useState } from "react";
import accountService from "../../apis/account.api";
import cartService from "../../apis/cart.api";
import { connect } from "react-redux";
import { fetchProfile } from "../../redux/actions/customerActions";
import { bindActionCreators } from "redux";
import { useToasts } from "react-toast-notifications";
import { normalRoute } from "../../components/withPrivateRoute";
import { fetchCarts } from "../../redux/actions/cartActions";

const Login = () => {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { addToast } = useToasts();

  const ilogin = async () => {
    await accountService
      .login({
        email,
        password,
      })
      .then(async (data) => {
        addToast("Đăng nhập thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        router.push("/");
      })
      .catch((error) => {
        addToast("Đăng nhập thất bại!", {
          appearance: "error",
          autoDismiss: true,
        });
      });
  };
  return (
    <LayoutSix>
      {/* breadcrumb */}
      {/* <BreadcrumbOne pageTitle="Login">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Home</a>
            </Link> 
          </li>
          <li className="breadcrumb-item active">Login</li>
        </ol>
      </BreadcrumbOne> */}
      <div className="login-content space-pt--20 space-pb--r100">
        <Container>
          <Row className="justify-content-center">
            <Col xl={6} md={10}>
              <div className="login-wrap">
                <div className="heading-s1 space-mb--20">
                  <h3>Đăng nhập</h3>
                </div>
                <div>
                  <form method="post">
                    <div className="form-group">
                      <input
                        type="text"
                        required
                        className="form-control"
                        name="email"
                        placeholder="Nhập Email"
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </div>
                    <div className="form-group">
                      <input
                        className="form-control"
                        required
                        type="password"
                        name="password"
                        placeholder="Mật khẩu từ 6 đến 32 ký tự"
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </div>
                    <div className="login-footer form-group">
                      <div className="check-form">
                        {/* <div className="custom-checkbox">
                          <input
                            className="form-check-input"
                            type="checkbox"
                            name="checkbox"
                            id="exampleCheckbox1"
                            defaultValue
                          />
                          <label
                            className="form-check-label"
                            htmlFor="exampleCheckbox1"
                          >
                            <span>Remember me</span>
                          </label>
                        </div> */}
                      </div>
                      <a href="#">Quên mật khẩu?</a>
                    </div>
                    <div className="form-group">
                      <button
                        type="submit"
                        className="btn btn-fill-out btn-block"
                        name="login"
                        onClick={(e) => {
                          e.preventDefault();
                          ilogin();
                        }}
                      >
                        Đăng nhập
                      </button>
                    </div>
                  </form>
                  <div className="different-login">
                    <span> Hoặc</span>
                  </div>
                  <ul className="btn-login text-center">
                    <li>
                      <a href="#" className="btn btn-facebook">
                        <FaFacebookF />
                        Facebook
                      </a>
                    </li>
                    <li>
                      <a href="#" className="btn btn-google">
                        <FaGooglePlusG />
                        Google
                      </a>
                    </li>
                  </ul>
                  <div className="form-note text-center space-mt--20">
                    Bạn mới đến Rent Mart?{" "}
                    <Link href="/renter/register">
                      <a>Đăng ký</a>
                    </Link>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </LayoutSix>
  );
};

// const mapDispatchToProps = (dispatch) =>
//   bindActionCreators(
//     {
//       getProfile: fetchProfile,
//       getCarts: fetchCarts,
//     },
//     dispatch
//   );

export default normalRoute(Login);
