import LayoutProfile from "../../../components/Account/LayoutProfile";
import orderService from "../../../apis/order.api";
import { useEffect, useState } from "react";
import Link from "next/link";
import CardLoad from "../../../components/Preloader/cardLoad";
import { privateRoute } from "../../../components/withPrivateRoute";
import { formatDate, formatPrice, orderStatus } from "../../../lib/utilities";
import { useRouter } from "next/router";
import Paginator from "react-hooks-paginator";

const OrderHistoryPage = () => {
  const router = useRouter();
  const [orders, setOrders] = useState([]);
  const [ordersFilter, setOrderFilter] = useState([]);
  const [loading, setLoading] = useState(true);
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentData, setCurrentData] = useState([]);
  const pageLimit = 10;

  useEffect(() => {
    if (orders) {
      setCurrentData(ordersFilter?.slice(offset, offset + pageLimit));
    }
  }, [offset, orders, ordersFilter]);

  useEffect(() => {
    async function getMyOrder() {
      await orderService
        .getMyOrder()
        .then((response) => {
          setOrders(response);
          setOrderFilter(response);
          setLoading(false);
        })
        .catch(() => {
          router.push("not-found");
        });
    }
    getMyOrder();
  }, []);
  return (
    <LayoutProfile
      title="Đơn hàng của tôi | Rent Mart "
      breadcrumbName="Đơn hàng của tôi"
      activeKey="order"
      orders={orders}
      setOrderFilter={setOrderFilter}
    >
      <div className="myaccount-table table-responsive text-center">
        {ordersFilter.length > 0 && (
          <table className="table">
            <thead>
              <tr>
                <th>Mã đơn hàng</th>
                <th>Ngày thuê</th>
                <th>Sản phẩm</th>
                <th>Tổng tiền</th>
                <th>Trạng tái đơn hàng</th>
              </tr>
            </thead>
            <tbody>
              {currentData?.map((item, key) => (
                <tr key={key} className="order-item">
                  <td className="order-code">
                    <Link
                      href={{
                        pathname: "/renter/order/view/[id]",
                        query: {
                          id: item._id,
                        },
                      }}
                    >
                      <a style={{ color: "#007bff" }}>{item._id.slice(0, 9)}</a>
                    </Link>
                  </td>
                  <td>{formatDate(item.history.waitingToConfirm)}</td>
                  <td style={{ width: "35%" }}>{item.product.name}</td>
                  <td>{formatPrice(item.totalPrice)} đ</td>
                  <td>
                    <p className={item.status}>{orderStatus(item.status)}</p>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
        {loading && <CardLoad />}
        {!loading && currentData.length === 0 && (
          <p> Bạn chưa đơn hàng nào </p>
        )}
      </div>
      <div className="pagination pagination-style pagination-style--two justify-content-center">
        <Paginator
          totalRecords={ordersFilter.length}
          pageLimit={pageLimit}
          pageNeighbours={1}
          setOffset={setOffset}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
          pageContainerClass="mb-0 mt-0"
          pagePrevText="«"
          pageNextText="»"
        />
      </div>
    </LayoutProfile>
  );
};

export default privateRoute(OrderHistoryPage);
