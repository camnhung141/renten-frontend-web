import LayoutProfile from "../../../../components/Account/LayoutProfile";
import orderService from "../../../../apis/order.api";
import { Fragment,useEffect, useState } from "react";
import { IoMdAdd } from "react-icons/io";
import Link from "next/link";
import CardLoad from "../../../../components/Preloader/cardLoad";
import { privateRoute } from "../../../../components/withPrivateRoute";
import {
  formatDate,
  formatPrice,
  getAddress,
  orderStatus,
  formatDateAndHour,
  paymentMethod,
  historyTracking,
  getDateVN,
  dateStatusEnd,
} from "../../../../lib/utilities";
import { useRouter } from "next/router";

const TrackingOrder = () => {
  const router = useRouter();
  const [order, setOrder] = useState(null);
  const [times, setTimes] = useState([]);
  useEffect(() => {
    async function getMyOrder() {
      await orderService
        .getOrderById(router.query.id)
        .then((response) => {
          if (response.status === "cancel") router.push("/not-found");
          else {
            setOrder(response);
            setTimes(historyTracking(response.history));
          }
        })
        .catch(() => {
          router.push("/not-found");
        });
    }
    getMyOrder();
  }, []);

  return (
    <LayoutProfile
      title="Đơn hàng của tôi | Rent Mart "
      breadcrumbName="Đơn hàng của tôi"
      activeKey="order"
    >
      {order ? (
        <div className="tracking edAZXd">
          <div className="bOXEez">
            <div className="order-heading">
              <h3>Theo dõi đơn hàng #{order._id}</h3>
              {/* <div className="view-detail">Xem chi tiết đơn hàng</div> */}
              <a
                className="view-detail"
                href={`/renter/order/view/${order._id}`}
              >
                Xem chi tiết đơn hàng
              </a>
            </div>
            <div>
              <div className="cVeNsR">
                <p className="title">Trạng thái đơn hàng</p>
                <div className="status-block">
                  <span className="status-text">
                    Trạng thái:
                    <b>&nbsp;{dateStatusEnd(order.history, order.status)}</b>
                  </span>
                  <div className="order-progress-bar">
                    <div
                      className={
                        order.history.waitingToConfirm
                          ? order.status === "waitingToConfirm"
                            ? "cKEwuh"
                            : "hMlakU"
                          : "hkLMUa"
                      }
                    >
                      <span>Đặt hàng thành công</span>
                    </div>
                    <div
                      className={
                        order.history.processing
                          ? order.status === "processing"
                            ? "cKEwuh"
                            : "hMlakU"
                          : "hkLMUa"
                      }
                    >
                      <span>Đơn hàng được xác nhận</span>
                    </div>
                    <div
                      className={
                        order.history.transporting
                          ? order.status === "transporting"
                            ? "cKEwuh"
                            : "hMlakU"
                          : "hkLMUa"
                      }
                    >
                      <span>Đơn hàng đang vận chuyển</span>
                    </div>
                    <div
                      className={
                        order.history.rented
                          ? order.status === "rented"
                            ? "cKEwuh"
                            : "hMlakU"
                          : "hkLMUa"
                      }
                    >
                      <span>Người thuê đã nhận hàng</span>
                    </div>
                    {/*<div className="hMlakU">
                      <span>Bàn giao vận chuyển</span>
                    </div>
                     <div className="hMlakU">
                      <span>Đang vận chuyển</span>
                    </div>*/}
                    <div
                      className={order.history.returned ? "cKEwuh" : "cKEwul"}
                    >
                      <span>Hoàn trả thành công</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="cskwFI">
                <p className="title">Chi tiết trạng thái đơn hàng</p>
                <div className="detail">
                  <div className="border">
                    {times.map((item, index) => (
                      <Fragment key = {index}>
                        <div key={index} className={"cGrxj"}>
                          {index === 0 && "Cập nhật mới nhất:"} {item.date}
                        </div>
                        {item.time.map((time, i) => (
                          <div
                            key={index + i}
                            className={
                              index === 0 && i === 0 ? "cBkDRy" : "dPnPGf"
                            }
                          >
                            <span className="left">{time.hour}</span>
                            <span className="right">{time.title}</span>
                          </div>
                        ))}
                      </Fragment>
                    ))}
                  </div>
                </div>
              </div>
              <div className="LgwZV">
                <p className="title">Kiện hàng gồm có</p>
                <div className="items">
                  <div className="item">
                    <img className="image" src={order.product.thumbnail} />
                    <div className="info">
                      <a
                        className="name"
                        href="/product-p299461.html?spid=299484"
                      >
                        {order.product.name}
                      </a>
                      <div className="seller">
                        Bán và giao bởi <span>{order.agency.agencyName}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <CardLoad />
      )}
    </LayoutProfile>
  );
};

export default privateRoute(TrackingOrder);
