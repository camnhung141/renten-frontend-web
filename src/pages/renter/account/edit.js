import LayoutProfile from "../../../components/Account/LayoutProfile";
import { connect } from "react-redux";
import Profile from "../../../components/Account/Profile";
import { useToasts } from "react-toast-notifications";
import { updateToCustomer } from "../../../redux/actions/customerActions";
import {privateRoute} from "../../../components/withPrivateRoute";
const MyAccount = ({ user, updateCustomer }) => {
  const { addToast } = useToasts();
  return (
    <LayoutProfile
      title="Tài khoản của tôi | Rent Mart "
      breadcrumbName="Thông tin tài khoản"
      activeKey="accountDetails"
    >
     { user && <Profile
        user={user}
        addToast={addToast}
        updateCustomer={updateCustomer}
      />}
    </LayoutProfile>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCustomer: (item, addToast) => {
      dispatch(updateToCustomer(item, addToast));
    },
  };
};

export default privateRoute(
  connect(mapStateToProps, mapDispatchToProps)(MyAccount)
);
