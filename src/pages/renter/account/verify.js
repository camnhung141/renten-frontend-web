import LayoutProfile from "../../../components/Account/LayoutProfile";
import { connect } from "react-redux";
import Verify from "../../../components/Account/Verify";
import { useToasts } from "react-toast-notifications";
import { privateRoute } from "../../../components/withPrivateRoute";
import { fetchProfile } from "../../../redux/actions/customerActions";
import { bindActionCreators } from "redux";

const VerifyPage = ({ user, getProfile }) => {
  const { addToast } = useToasts();
  return (
    <LayoutProfile
      title="Xác minh tài khoản | Rent Mart "
      breadcrumbName="Xác minh tài khoản"
      activeKey="verify"
    >
      {user && (
        <Verify user={user} addToast={addToast} getProfile={getProfile} />
      )}
    </LayoutProfile>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getProfile: fetchProfile,
    },
    dispatch
  );

export default privateRoute(
  connect(mapStateToProps, mapDispatchToProps)(VerifyPage)
);
