import Address from "../../../components/Account/Address";
import LayoutProfile from "../../../components/Account/LayoutProfile";
import customerService from "../../../apis/customer.api";
import { useEffect, useState } from "react";
import { IoMdAdd } from "react-icons/io";
import Link from "next/link";
import CardLoad from "../../../components/Preloader/cardLoad";
import { privateRoute } from "../../../components/withPrivateRoute";

const NotificationPage = () => {
  
  return (
    <LayoutProfile
      title="Thông báo của tôi | Rent Mart "
      breadcrumbName="Thông báo của tôi"
      activeKey="notice"
    >
      
    </LayoutProfile>
  );
};

export default privateRoute(NotificationPage);
