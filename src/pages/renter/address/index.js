import Address from "../../../components/Account/Address";
import LayoutProfile from "../../../components/Account/LayoutProfile";
import customerService from "../../../apis/customer.api";
import { useEffect, useState } from "react";
import { IoMdAdd } from "react-icons/io";
import Link from "next/link";
import CardLoad from "../../../components/Preloader/cardLoad";
import { privateRoute } from "../../../components/withPrivateRoute";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";

const AddressPage = ({ user }) => {
  const [addressList, setAddressList] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    async function getDeliveryAddress() {
      await customerService
        .getDeliveryAddress()
        .then((response) => {
          setLoading(false);
          setAddressList(response.data.payload);
        })
        .catch();
    }
    getDeliveryAddress();
  }, []);
  const { addToast } = useToasts();

  const handleDelete = async (id) => {
    await customerService
      .deleteDeliverAddress(user._id, id)
      .then(() => {
        setAddressList(addressList.filter((item) => item._id !== id));
        addToast("Xoá địa chỉ thành công", {
          appearance: "success",
          autoDismiss: true,
        });
      })
      .catch(() => {
        addToast("Xoá địa chỉ thất bại", {
          appearance: "error",
          autoDismiss: true,
        });
      });
  };
  return (
    <LayoutProfile
      title="Sổ địa chỉ (Address book) | Rent Mart "
      breadcrumbName="Sổ địa chỉ"
      activeKey="address"
    >
      <Link href="address/[address]" as="address/create">
        <a className="address new">
          {" "}
          <IoMdAdd /> Thêm địa chỉ mới
        </a>
      </Link>
      {addressList.length > 0 &&
        addressList.map((address) => (
          <Address
            key={address._id}
            address={address}
            handleDelete={() => handleDelete(address._id)}
          />
        ))}
      {!loading && addressList.length === 0 && <p>Bạn chưa có địa chỉ nào.</p>}
      {loading && <CardLoad />}
    </LayoutProfile>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.userData.user,
  };
};
export default privateRoute(connect(mapStateToProps)(AddressPage));
