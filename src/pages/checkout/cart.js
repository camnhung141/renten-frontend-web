import { useState, Fragment, useEffect } from "react";
import Link from "next/link";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";
import {
  addToCart,
  decreaseQuantity,
  deleteFromCart,
  deleteAllFromCart,
  updateFromCart,
  fetchCarts,
} from "../../redux/actions/cartActions";
import { getDiscountPrice } from "../../lib/product";
import { Container, Row, Col } from "react-bootstrap";
import { LayoutSix } from "../../layouts";
import { BreadcrumbOne } from "../../components/Breadcrumb";
import { IoIosClose } from "react-icons/io";
import { BsClock } from "react-icons/bs";
import ModalTimeRental from "../../components/Product/ModalTimeRental";
import { formatPrice, getTime } from "../../lib/utilities";

const Cart = ({
  user,
  cartItems,
  deleteFromCart,
  deleteAllFromCart,
  updateFromCart,
  getCarts,
}) => {
  const [quantityCount] = useState(1);
  const { addToast } = useToasts();
  let cartTotalPrice = 0;
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const [cartItem, setCartItem] = useState(null);
  const handleShow = (item) => {
    setCartItem(item);
    setShow(true);
  };
  const handleUpdate = (time) => {
    updateFromCart(user, cartItem, time, addToast);
  };
  useEffect(() => {
    getCarts();
  }, []);
  return (
    <LayoutSix title="Giỏ hàng | Rent Mart">
      {/* breadcrumb */}
      <BreadcrumbOne pageTitle="Shopping Cart">
        <ol className="breadcrumb justify-content-md-start">
          <li className="breadcrumb-item">
            <Link href="/">
              <a>Trang chủ</a>
            </Link>
          </li>
          <li className="breadcrumb-item active">Giỏ hàng</li>
        </ol>
      </BreadcrumbOne>
      {/* cart content */}
      <div className="cart-content space-pt--20 space-pb--r100">
        <Container>
          {cartItems && cartItems.length >= 1 ? (
            <Fragment>
              <Row>
                <Col lg={12}>
                  <div className="table-responsive shop-cart-table">
                    <table className="table mb-0">
                      <thead>
                        <tr>
                          <th className="product-thumbnail">&nbsp;</th>
                          <th className="product-name">Sản phẩm</th>
                          <th className="product-price">Giá</th>
                          <th className="product-time-rental">
                            Thời gian thuê
                          </th>
                          <th className="product-quantity">
                            Tổng thời gian thuê
                          </th>
                          <th className="product-subtotal">Tổng</th>
                          <th className="product-remove text-center">Xoá</th>
                        </tr>
                      </thead>
                      <tbody>
                        {cartItems.map((item, key) => {
                          const dayBetween = getTime(
                            new Date(item.beginTime),
                            new Date(item.endTime),
                            item.template.rentDetail.unitRent
                          );
                          cartTotalPrice += item.product.rentPrice * dayBetween;
                          return (
                            <tr key={key}>
                              <td
                                className="product-thumbnail"
                                data-title="Ảnh"
                              >
                                <Link
                                  href={{
                                    pathname: "/product/[slug]",
                                    query: {
                                      slug: `${item.template.name.replaceAll(
                                        " ",
                                        "-"
                                      )}`,
                                      spid: item.templateId,
                                    },
                                  }}
                                >
                                  <a>
                                    <img
                                      src={
                                        item.product.thumbnail ||
                                        "/assets/images/product/fashion/1.jpg"
                                      }
                                      alt="product1"
                                    />
                                  </a>
                                </Link>
                              </td>
                              <td
                                className="product-name w-25"
                                data-title="Sản phẩm"
                              >
                                <div>
                                  <Link
                                    href={{
                                      pathname: "/product/[slug]",
                                      query: {
                                        slug: `${item.template.name.replaceAll(
                                          " ",
                                          "-"
                                        )}`,
                                        spid: item.templateId,
                                      },
                                    }}
                                  >
                                    <a>{item.template.name}</a>
                                  </Link>
                                  <div className="cart-variation">
                                    <p>
                                      Mã sản phẩm: {item.product.productCode}
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td className="product-price" data-title="Giá">
                                {formatPrice(item.product.rentPrice)}đ
                              </td>

                              <td
                                className="product-time-rental"
                                data-title="Thời gian thuê"
                              >
                                {new Date(item.beginTime).toLocaleString(
                                  "vn-VN"
                                )}{" "}
                                -{" "}
                                {new Date(item.endTime).toLocaleString("vn-VN")}
                                <button onClick={() => handleShow(item)}>
                                  <BsClock size={20} />
                                </button>
                              </td>
                              <td
                                className="product-quantity"
                                data-title="Tổng thời gian thuê"
                              >
                                <div className="cart-plus-minus">
                                  {dayBetween}{" "}
                                  {item.template.rentDetail.unitRent === "date"
                                    ? "Ngày"
                                    : "Giờ"}
                                </div>
                              </td>
                              <td
                                className="product-subtotal"
                                data-title="Tổng"
                              >
                                {formatPrice(
                                  dayBetween * item.product.rentPrice
                                )}
                                đ
                              </td>
                              <td className="product-remove">
                                <button
                                  onClick={() =>
                                    deleteFromCart(user, item.product, addToast)
                                  }
                                >
                                  <IoIosClose />
                                </button>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colSpan="6" className="px-0 pb-0">
                            <Row className="no-gutters align-items-center">
                              <Col lg={4} md={6} className="mb-3 mb-md-0">
                                <div className="coupon field-form input-group">
                                  <input
                                    type="text"
                                    className="form-control form-control-sm"
                                    placeholder="Nhập mã khuyễn mãi.."
                                  />
                                  <div className="input-group-append">
                                    <button
                                      className="btn btn-fill-out btn-sm"
                                      type="submit"
                                    >
                                      Áp dụng khuyễn mãi
                                    </button>
                                  </div>
                                </div>
                              </Col>
                              <Col
                                lg={8}
                                md={6}
                                className="text-left text-md-right"
                              >
                                <button
                                  className="btn btn-line-fill btn-sm"
                                  type="submit"
                                  onClick={() =>
                                    deleteAllFromCart(user, addToast)
                                  }
                                >
                                  Xoá tất cả
                                </button>
                              </Col>
                            </Row>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </Col>
              </Row>
              <Row>
                <Col lg={12}>
                  <div className="divider center-icon space-mt--30 space-mb--30">
                    <i className="icon-basket-loaded" />
                  </div>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <div className="border p-3 p-md-4">
                    <div className="heading-s1 mb-3">
                      <h6>Tổng đơn hàng</h6>
                    </div>
                    <div className="table-responsive">
                      <table className="table">
                        <tbody>
                          <tr>
                            <td className="cart-total-label">Tạm tính</td>
                            <td className="cart-total-amount">
                              {formatPrice(cartTotalPrice)}đ
                            </td>
                          </tr>
                          <tr>
                            <td className="cart-total-label">Phí vận chuyển</td>
                            <td className="cart-total-amount">Free Shipping</td>
                          </tr>
                          <tr>
                            <td className="cart-total-label">Thành tiền</td>
                            <td className="cart-total-amount">
                              <strong>{formatPrice(cartTotalPrice)}đ</strong>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <Link href="/checkout/payment">
                      <a className="btn btn-fill-out">Tiến hành thuê</a>
                    </Link>
                  </div>
                </Col>
              </Row>
            </Fragment>
          ) : (
            <Row>
              <Col>
                <div className="item-empty-area text-center">
                  <div className="item-empty-area__icon space-mb--30">
                    <i className="icon-basket-loaded" />
                  </div>
                  <div className="item-empty-area__text">
                    <p className="space-mb--30">Giỏ hàng rỗng</p>
                    <Link href="/">
                      <a className="btn btn-fill-out">Thuê ngay</a>
                    </Link>
                  </div>
                </div>
              </Col>
            </Row>
          )}
        </Container>
      </div>
      <ModalTimeRental
        show={show}
        handleClose={handleClose}
        handleSave={handleUpdate}
      />
    </LayoutSix>
  );
};

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartData,
    user: state.userData.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCarts: () => {
      dispatch(fetchCarts());
    },
    addToCart: (item, addToast, quantityCount) => {
      dispatch(addToCart(item, addToast, quantityCount));
    },
    decreaseQuantity: (item, addToast) => {
      dispatch(decreaseQuantity(item, addToast));
    },
    deleteFromCart: (user, item, addToast) => {
      dispatch(deleteFromCart(user, item, addToast));
    },
    updateFromCart: (user, item, time, addToast) => {
      dispatch(updateFromCart(user, item, time, addToast));
    },
    deleteAllFromCart: (user, addToast) => {
      dispatch(deleteAllFromCart(user, addToast));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
