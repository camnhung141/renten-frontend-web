import { connect } from "react-redux";
import { getProducts } from "../lib/product";
import { LayoutFive } from "../layouts";
import { HeroSliderFive } from "../components/HeroSlider";
import { BannerFive } from "../components/Banner";
import { ProductTabThree } from "../components/ProductTab";
import { BrandLogoThree } from "../components/BrandLogo";
import {
  ProductSliderEightWrapper,
  ProductSliderNine,
  DealProductSlider,
} from "../components/ProductSlider";

import heroSliderFiveData from "../data/hero-sliders/hero-slider-five.json";
import brandLogoData from "../data/brand-logo/brand-logo-one.json";
import productService from "../apis/product.api";

const ElectronicsOne = ({
  bestRenterProducts,
  featuredProducts,
  featuredProductsTwo,
  bestSellerProductsTwo,
  trendingProducts,
  saleProducts,
  saleProductsTwo,
  newProducts,
  dealProducts,
}) => {
  return (
    <LayoutFive navPositionClass="justify-content-start">
      {/* hero slider */}
      <HeroSliderFive heroSliderData={heroSliderFiveData} />
      {/* tab product */}
      <ProductTabThree
        title="Nổi bật"
        bannerImage="/assets/images/banner/shop_banner_img6.jpg"
        newProducts={newProducts}
        bestRenterProducts={bestRenterProducts}
        featuredProducts={featuredProducts}
        saleProducts={bestRenterProducts}
      />
      {/* banner */}
      <BannerFive containerClass="custom-container" />
      {/* deal products */}
      {/* <DealProductSlider title="Deal Of The Day" products={dealProducts} /> */}
      {/* product slider */}
      <ProductSliderNine
        title="Sản phẩm thịnh hành"
        bannerImage="/assets/images/banner/shop_banner_img10.jpg"
        products={bestRenterProducts}
      />
      {/* brand logo */}
      {/* <BrandLogoThree title="Our Brands" brandLogoData={brandLogoData} /> */}
      {/* product slider */}
      <ProductSliderEightWrapper
        featuredTitle="Mới nhất"
        bestSellerTitle="Thuê nhiều nhất"
        saleTitle="Đánh giá cao"
        featuredProducts={featuredProducts}
        bestRenterProducts={bestRenterProducts}
        saleProducts={newProducts}
      />
    </LayoutFive>
  );
};
export default ElectronicsOne;

export async function getServerSideProps() {
  try {
    const data = await productService.getProducts({
      sort: "rentCount:-1",
      limit: 9,
    });
    const bestRenterProducts = data.products;
    const data2 = await productService.getProducts({
      sort: "rate.rateAverage:-1",
      limit: 9,
    });
    const featuredProducts = data2.products;
    const data3 = await productService.getProducts({
      sort: "createdAt:-1",
      limit: 9,
    });
    const newProducts = data3.products;
    return { props: { bestRenterProducts, featuredProducts, newProducts } };
  } catch {
    return { props: {} };

    // return {
    //   redirect: {
    //     destination: "/not-found",
    //     permanent: false,
    //   },
    // };
  }
}
