import axiosApiInstance from "../lib/axiosApiInstance";

const API_URL = process.env.BACKEND_URL+ "orders/";
class OrderService {
  getMyOrder() {
    return axiosApiInstance.get(API_URL).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }
  getOrderById(id) {
    return axiosApiInstance.get(API_URL + id).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }
  updateOrderById(id, status) {
    return axiosApiInstance
      .put(API_URL + id, { status: status, dateNotif: new Date() })
      .then((response) => {
        const idata = response.data.payload;
        return idata;
      });
  }
}
export default new OrderService();
