import axios from "axios";

const API_URL = process.env.BACKEND_URL+ "product-templates/";
// const cookie = new Cookies();

class ProductService {
  list() {
    return axios.get(API_URL).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }

  getDetailProduct(id) {
    return axios.get(API_URL + id).then(async (response) => {
      return response.data.payload;
    });
  }

  getRatingProducts(id) {
    return axios.get(API_URL + id + "/products").then(async (response) => {
      return response.data.payload;
    });
  }

  getReviewProduct(id) {
    return axios.get(API_URL + id + "/reviews/").then(async (response) => {
      return response.data.payload;
    });
  }
  getProducts(payload) {
    let {
      categoriesId,
      keyword,
      page,
      subCategoriesId,
      sort,
      agencyId,
      limit,
    } = payload;
    return axios
      .get(API_URL, {
        params: {
          categoriesId: categoriesId,
          keyword: keyword,
          page: page,
          subCategoriesId: subCategoriesId,
          sort: sort,
          agencyId: agencyId,
          limit: limit,
        },
      })
      .then((response) => {
        return response.data.payload;
      })
      .catch(() => {
        return null;
      });
  }
}
export default new ProductService();
