import axios from "axios";

const API_URL = process.env.BACKEND_URL+ "agencies/";
class ShopService {
  getShopDetail(id) {
    return axios
      .get(API_URL + id)
      .then((response) => {
        const idata = response.data.payload;
        return idata;
      })
      .catch((error) => {
        return null;
      });
  }
}
export default new ShopService();
