import axiosApiInstance from '../lib/axiosApiInstance';

const API_URL = process.env.BACKEND_URL + "products/";

class ReviewService {
  reviews(id, payload) {
    return axiosApiInstance.post(API_URL + id + "/reviews", payload).then((response) => {
      const idata = response.data.payload;
      return idata;
    });
  }
}
export default new ReviewService();
