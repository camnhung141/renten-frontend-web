import { v4 as uuidv4 } from "uuid";
import {
  FETCH_PROFILE_USER_SUCCESS,
  FETCH_PROFILE_USER_PENDING,
  FETCH_PROFILE_USER_ERROR,
  FETCH_PROFILE_LOGOUT_SUCCESS
} from "../actions/customerActions";

const initState = [];

const customerReducer = (state = initState, action) => {
  if (action.type === FETCH_PROFILE_USER_SUCCESS) {
    return {
      ...state,
      pending: false,
      user: action.payload,
      error: false,
    };
  }
  if (action.type === FETCH_PROFILE_LOGOUT_SUCCESS) {
    return {
      ...state,
      pending: false,
      user: undefined,
      error: false,
    };
  }
  if (action.type === FETCH_PROFILE_USER_PENDING) {
    return {
      ...state,
      pending: true,
      error: false,
    };
  }
  if (action.type === FETCH_PROFILE_USER_ERROR) {
    return {
      ...state,
      pending: false,
      error: action.payload,
    };
  }

  return state;
};

export default customerReducer;
