import customerService from "../../apis/customer.api";

export const UPDATE_FROM_CUSTOMER = "UPDATE_FROM_CUSTOMER";
export const FETCH_PROFILE_USER_SUCCESS = "FETCH_PROFILE_USER_SUCCESS";
export const FETCH_PROFILE_USER_PENDING = "FETCH_PROFILE_USER_PENDING";
export const FETCH_PROFILE_USER_ERROR = "FETCH_PROFILE_USER_ERROR";
export const FETCH_PROFILE_LOGOUT_SUCCESS = "FETCH_PROFILE_LOGOUT_SUCCESS";

const fetchProfileSuccess = (user) => ({
  type: FETCH_PROFILE_USER_SUCCESS,
  payload: user,
});

const fetchProfileLogoutSuccess = () => ({
  type: FETCH_PROFILE_LOGOUT_SUCCESS,
});

const fetchProfilePending = () => ({
  type: FETCH_PROFILE_USER_PENDING,
});

const fetchProfileError = (error) => ({
  type: FETCH_PROFILE_USER_ERROR,
  payload: error,
});

export const logout = () => {
  return (dispatch) => {
    dispatch(fetchProfileLogoutSuccess());
  };
};

export const fetchProfile = () => {
  return (dispatch) => {

    dispatch(fetchProfilePending());
    customerService
      .getProfile()
      .then((data) => {
        var user = data.data.payload;
        delete user.accountId.password;
        dispatch(fetchProfileSuccess(user));
        return user;
      })
      .catch((error) => {
        dispatch(fetchProfileSuccess(null));
        dispatch(fetchProfileError(error));
      });
  };
};
// update to Customer
export const updateToCustomer = (item, addToast) => {
  return (dispatch) => {
    customerService
      .updateProfile(item)
      .then((data) => {
        addToast("Cập nhật thành công!", {
          appearance: "success",
          autoDismiss: true,
        });
        dispatch({ type: FETCH_PROFILE_USER_SUCCESS, payload: item });
      })
      .catch((error) => {
        addToast("Cập nhật thật bại", {
          appearance: "error",
          autoDismiss: true,
        });
      });
  };
};
