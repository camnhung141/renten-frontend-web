import cartService from "../../apis/cart.api";

export const ADD_TO_CART = "ADD_TO_CART";
export const DECREASE_QUANTITY = "DECREASE_QUANTITY";
export const DELETE_FROM_CART = "DELETE_FROM_CART";
export const DELETE_ALL_FROM_CART = "DELETE_ALL_FROM_CART";
export const FETCH_CARTS_SUCCESS = "FETCH_CARTS_SUCCESS";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const UPDATE_FROM_CART = "UPDATE_FROM_CART";

const fetchCartsSuccess = (carts) => ({
  type: FETCH_CARTS_SUCCESS,
  payload: carts,
});

const logoutSuccess = () => ({
  type: LOGOUT_SUCCESS,
});

export const logoutCart = () => {
  return (dispatch) => {
    dispatch(logoutSuccess());
  };
};

export const fetchCarts = () => {
  return (dispatch) => {
    cartService
      .getMyCarts()
      .then((data) => {
        const carts = data;
        dispatch(fetchCartsSuccess(carts));
        return carts;
      })
      .catch((error) => {
        dispatch(fetchCartsSuccess(null));
      });
  };
};
//Chọn Thuê
export const addToCart = (
  user,
  item,
  addToast,
  timeRental,
  selectedProductColor,
  selectedProductSize
) => {
  return (dispatch) => {
    if (!timeRental.endTime) {
      addToast("Vui lòng chọn thời gian thuê.", {
        appearance: "warning",
        autoDismiss: true,
      });
    } else {
      if (user) {
        const data = {
          productId: item._id,
          beginTime: timeRental.beginTime,
          endTime: timeRental.endTime,
        };
        cartService
          .addToMyCart(data)
          .then((response) => {
            addToast("Đã thêm vào giỏ hàng.", {
              appearance: "success",
              autoDismiss: true,
            });
            var cartItem = response.data.payload;
            // cartItem["product"] = item;
            dispatch({
              type: ADD_TO_CART,
              payload: cartItem,
            });
          })
          .catch((error) => {
            addToast("Thêm vào giỏ hàng không đươc thực hiện.", {
              appearance: "error",
              autoDismiss: true,
            });
          });
      } else {
        addToast("Đã thêm vào giỏ hàng.", {
          appearance: "success",
          autoDismiss: true,
        });
        dispatch({
          type: ADD_TO_CART,
          payload: {
            beginTime: timeRental.beginTime,
            endTime: timeRental.endTime,
            product: item,
            productId: item._id,
          },
        });
      }
    }
  };
};
//decrease from cart
export const decreaseQuantity = (item, addToast) => {
  return (dispatch) => {
    if (addToast) {
      addToast("Item Decremented From Cart", {
        appearance: "warning",
        autoDismiss: true,
      });
    }
    dispatch({ type: DECREASE_QUANTITY, payload: item });
  };
};
//delete from cart
export const deleteFromCart = (user, item, addToast) => {
  return (dispatch) => {
    if (user) {
      cartService
        .updateCart({
          productId: item._id,
          quantity: 0,
        })
        .then((res) => {
          dispatch({ type: DELETE_FROM_CART, payload: item });
          addToast("Đã xoá khỏi giỏ hàng", {
            appearance: "error",
            autoDismiss: true,
          });
        })
        .catch(() => {
          addToast("Thất bại", {
            appearance: "warning",
            autoDismiss: true,
          });
        });
    } else {
      addToast("Đã xoá khỏi giỏ hàng", {
        appearance: "error",
        autoDismiss: true,
      });
      dispatch({ type: DELETE_FROM_CART, payload: item });
    }
  };
};
//delete all from cart
export const deleteAllFromCart = (user, addToast) => {
  return (dispatch) => {
    if (user) {
      cartService
        .deleteCart()
        .then(() => {
          if (addToast) {
            addToast("Đã xóa tất cả khỏi giỏ hàng", {
              appearance: "error",
              autoDismiss: true,
            });
          }
          dispatch({ type: DELETE_ALL_FROM_CART });
        })
        .catch(() => {
          if (addToast) {
            addToast("Không thực hiện được xoá giỏ hàng", {
              appearance: "error",
              autoDismiss: true,
            });
          }
        });
    } else {
      if (addToast) {
        addToast("Đã xóa tất cả khỏi giỏ hàng", {
          appearance: "error",
          autoDismiss: true,
        });
      }
      dispatch({ type: DELETE_ALL_FROM_CART });
    }
  };
};

// get stock of cart item
export const cartItemStock = (item, color, size) => {
  if (item.stock) {
    return item.stock;
  } else {
    return item.variation
      .filter((single) => single.color === color)[0]
      .size.filter((single) => single.name === size)[0].stock;
  }
};

export const updateFromCart = (user, item, time, addToast) => {
  return (dispatch) => {
    if (user) {
      cartService
        .updateCart({
          productId: item.productId,
          beginTime: time[0],
          endTime: time[1],
          quantity: 1
        })
        .then((res) => {
          dispatch({ type: UPDATE_FROM_CART, payload: { item, time } });
          addToast("Sản phẩm cập nhật thành công", {
            appearance: "success",
            autoDismiss: true,
          });
        })
        .catch(() => {
          addToast("Sản phẩm cập nhật thất bại", {
            appearance: "warning",
            autoDismiss: true,
          });
        });
    } else {
      addToast("Sản phẩm cập nhật thành công", {
        appearance: "success",
        autoDismiss: true,
      });
      dispatch({ type: UPDATE_FROM_CART, payload: { item, time } });
    }
  };
};
