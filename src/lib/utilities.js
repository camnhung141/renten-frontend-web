import { eachDayOfInterval } from "date-fns";
export const getTime = (date1, date2, isTime) => {
  const difference = Math.abs(Math.ceil(date1.getTime() - date2.getTime()));
  const days = Math.ceil(difference / (1000 * 3600 * 24));
  if (isTime === "date") return Math.ceil(days);
  return Math.ceil(days) * 24;
};

export const getHour = (date1, date2) => {
  const difference = date1.getTime() - date2.getTime();
  const days = Math.ceil(difference / (1000 * 3600 * 24));
  return Math.ceil(days) * 24;
};

export const formatPrice = (number) => new Intl.NumberFormat().format(number);

export const formatDateAndHour = (date) =>
  new Date(date).toLocaleString("vn-VN", { hour12: false });

export const formatDate = (date) => new Date(date).toLocaleDateString();

export const orderStatus = (status) => {
  switch (status) {
    case "waitingToConfirm":
      return "Đợi xác nhận";
    case "processing":
      return "Đã xác nhận";
    case "transporting":
      return "Đang vận chuyển";
    case "waitingToReceive":
      return "Đang chờ thanh toán";
    case "rented":
      return "Đã thuê";
    case "returned":
      return "Đã hoàn trả";
    case "cancel":
      return "Đã huỷ";
    case "late":
      return "Đã hoàn trả";
    case "notReturned":
      return "Không hoàn trả";
    default:
      return null;
  }
};

export const paymentMethod = (method) => {
  switch (method) {
    case "cash":
      return "Thanh toán tiền mặt khi nhận hàng";
    case "momo":
      return "Thanh toán bằng ví MoMo";
    default:
      return null;
  }
};

export const getDateVN = (payload) => {
  var date = new Date(payload);
  var options = {
    weekday: "long",
    year: "numeric",
    month: "2-digit",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
  };
  return date.toLocaleString("vi", options);
};

export const getAddress = (address) => {
  if (address)
    return (
      address.streetAndNumber +
      ", " +
      address.wardName +
      ", " +
      address.districtName +
      ", " +
      address.cityName
    );
  return "";
};

export const historyTracking = (history) => {
  let arr = [];
  // {
  //   date: "",
  //   time: [
  //     {
  //       hour: "",
  //       title: "",
  //     },
  //   ],
  // },

  var options = {
    weekday: "long",
    year: "numeric",
    month: "2-digit",
    day: "numeric",
  };

  var optionsHour = {
    hour: "2-digit",
    minute: "2-digit",
  };

  const convertDate = (date) => new Date(date).toLocaleString("vi", options);
  const convertHour = (date) =>
    new Date(date).toLocaleString("vi", optionsHour);

  const addArray = (date, status) => {
    if (date) {
      const isExist = arr.find((item) => item.date === convertDate(date));
      if (isExist) {
        arr.forEach((item) => {
          if (item.date === convertDate(date)) {
            item.time.unshift({
              hour: convertHour(date),
              title: status,
            });
          }
        });
      } else {
        arr.unshift({
          date: convertDate(date),
          time: [
            {
              hour: convertHour(date),
              title: status,
            },
          ],
        });
      }
    }
  };
  addArray(history.waitingToConfirm, "Đặt hàng thành công");
  addArray(history.processing, "Đã xác nhận");
  addArray(history.transporting, "Đang vận chuyển");
  addArray(history.waitingToReceive, "Đang chờ thanh toán");
  addArray(history.rented, "Đã thuê");
  addArray(history.returned, "Đã hoàn trả");
  addArray(history.late, "Đã hoàn trả");
  addArray(history.notReturned, "Không hoàn trả");

  addArray(history.cancel, " Đã huỷ");
  return arr;
};
export const dateStatusEnd = (history, status) => {
  switch (status) {
    case "waitingToConfirm":
      // return {
      //   date: getDateVN(history.waitingToConfirm),
      //   title: "Đặt hàng thành công",
      // };
      return `Đặt hàng thành công | ${getDateVN(history.waitingToConfirm)}`;
    case "processing":
      return ` Đã xác nhận | ${getDateVN(history.processing)}`;
    case "transporting":
      return `Đang vận chuyển | ${getDateVN(history.transporting)}`;
    case "waitingToReceive":
      return `Đang chờ thanh toán | ${getDateVN(history.waitingToReceive)}`;
    case "rented":
      return `Đã thuê | ${getDateVN(history.rented)}`;
    case "returned":
      return `Đã hoàn trả | ${getDateVN(history.returned)}`;
    case "cancel":
      return ` Đã huỷ | ${getDateVN(history.cancel)}`;
    default:
      return null;
  }
};

export const ratingCount = (reviews, rate) => {
  return reviews.filter((item) => item.rating === rate).length;
};

export const dateRange = (ratingProducts, product) => {
  var dateDisable = [];
  ratingProducts.forEach((item) => {
    if (item._id === product._id) {
      item.orderedTime.forEach((time) => {
        var start = new Date(time.beginTime);
        var end = new Date(time.endTime);
        var range = eachDayOfInterval({ start: start, end: end });
        dateDisable.push(...range);
      });
    }
  });

  return dateDisable;
};
